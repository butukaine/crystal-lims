<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSresultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sresults', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('biopsy_no');
            $table->string('date_uploaded');
            $table->string('file');
            $table->string('comments')->nullable();
            $table->integer('number');
            $table->integer('search_id');
            $table->index('search_id')->references('id')->on('searches');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sresults');
    }
}
