<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('date_submitted');
            $table->string('date_responded')->nullable();

            $table->integer('case_id');
            $table->index('case_id')->references('id')->on('cases');
            $table->boolean('responded')->default(0);

            $table->integer('created_by');
            $table->index('created_by')->references('id')->on('users');

            $table->integer('response_id')->nullable();
            $table->index('response_id')->references('id')->on('responses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
