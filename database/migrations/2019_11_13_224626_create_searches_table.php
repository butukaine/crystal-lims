<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('searches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('gender')->nullable();
            $table->string('specimen')->nullable();
            $table->string('nationality')->nullable();
            $table->string('date_from')->nullable();
            $table->string('date_to')->nullable();
            $table->integer('age_start')->nullable();
            $table->integer('age_end')->nullable();
            $table->integer('searched_by');
            $table->index('searched_by')->references('id')->on('users');
            $table->integer('approved')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('searches');
    }
}
