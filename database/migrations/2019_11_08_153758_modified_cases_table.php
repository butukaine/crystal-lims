<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifiedCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('cases', function (Blueprint $table) {
            $table->string('nationality')->nullable();
            $table->string('village')->nullable()->change();
            $table->string('parish')->nullable()->change();
            $table->string('sub_county')->nullable()->change();
            $table->string('district')->nullable()->change();
            $table->string('clinical_summary')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
