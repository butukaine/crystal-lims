<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('biopsy_no');
            $table->integer('case_id');
            $table->index('case_id')->references('id')->on('cases');
            $table->integer('request_id');
            $table->index('request_id')->references('id')->on('requests');
            $table->string('physician_name');
            $table->string('physician_mobile');
            $table->string('physician_contact');
            $table->string('date_of_receipt');
            $table->text('clinical_summary');
            $table->text('ma_appearance');
            $table->text('mi_appearance');
            $table->text('conclusion');
            $table->string('document')->nullable();
            $table->boolean('submitted')->default(0);
            $table->string('submitted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
