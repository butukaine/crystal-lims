<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasesTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('cases', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('patient_name');
            $table->string('phys_surg');
            $table->integer('age');
            $table->string('gender');
            $table->string('patient_number');
            $table->string('health_unit');
            $table->string('village');
            $table->string('parish');
            $table->string('sub_county');
            $table->string('district');
            $table->string('specimen');
            $table->string('date_of_request');
            $table->string('date_of_receipt');
            $table->string('clinical_summary');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cases');
    }
}
