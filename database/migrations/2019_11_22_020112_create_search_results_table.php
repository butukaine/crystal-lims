<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('search_results', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('academician');
            $table->string('date_searched');
            $table->integer('search_id');
            $table->index('search_id')->references('id')->on('searches');
            $table->integer('case_id');
            $table->index('case_id')->references('id')->on('cases');
            $table->integer('response_id');
            $table->index('response_id')->references('id')->on('reports');
            $table->string('biopsy_no');
            $table->string('pathology_report');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('search_results');
    }
}
