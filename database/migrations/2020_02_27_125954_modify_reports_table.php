<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
   {
         Schema::table('reports', function ($table) {
            $table->string('physician_name')->nullable()->change();
            $table->string('physician_mobile')->nullable()->change();
            $table->string('physician_contact')->nullable()->change();
            $table->string('comments')->after('conclusion')->nullable();
            $table->string('health_unit')->after('date_of_receipt')->nullable();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
