<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Storage;
use Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';
    public function showRegistrationForm()
    {
       return view('auth.register')->with(['health_units'=>\App\HealthFacility::all()]);
    }
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        //$this->guard()->login($user);

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath())->with('message', 'Your information has been received. Wait for Admin approval to login.');
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'role' => 'required',
            'terms' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $request = app('request');
        $full_path = "";
        if($request->hasfile('photo')){
            $avatar = $request->file('photo');
            $path = $avatar->storeAs('/uploads/avatars', 'image_' . $data['email'] . '.' . $avatar->getClientOriginalExtension(), 'public');
            $full_path = env('APP_URL').'/storage/'. $path;
        }

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->account_state = 0;
        $user->password = Hash::make($request->password);
        $user->address = $request->address;
        $user->phone = $request->phone;
        $user->sex = $request->sex;
        $user->nationality = $request->nationality;
        $user->dob = $request->dob;
        $user->nin = $request->nin;
        $user->photo = $full_path;
        $user->save();


        $to_name = "Administrator";
        $to_email = "ben@kolastudios.com";
        $data = array('name'=> $user->name);
        
        Mail::send('emails.new_user', $data, function($message) use ($to_name, $to_email, $user) {
            $message->to($to_email, $to_name)
                    ->subject('New User Has Registered');
            $message->from($user->email,$user->name);
        });
        return $user;
    }
}
