<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    //  /**
    //  * Handle an authentication attempt.
    //  *
    //  * @param  \Illuminate\Http\Request $request
    //  *
    //  * @return Response
    //  */
    // public function authenticate(Request $request)
    // {
    //     $email = $request->email;
    //     $password = $request->password;

    //    if (Auth::attempt(['email' => $email, 'password' => $password, 'account_state' => 1])) {
    //     // The user is active, not suspended, and exists.

    //         return redirect()->route('home');
    //     }
    // }
 public function credentials(Request $request)
    {
        $credentials = $request->only($this->username(), 'password');
        $credentials = array_add($credentials, 'account_state', '1');
        return $credentials;
    } 


protected function attemptLogin(Request $request)
{
    //$request->merge(['account_state' => '1']);
    return $this->guard()->attempt(
        $this->credentials($request), $request->filled('remember')
    );
}
}