<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\HealthFacility;
use App\Submission;
use App\Response;
use Validator;
use Sentinel;
use Session;
use Image;
use App\PatientRequest;
use App\Report;
use Carbon\Carbon;
use App\PatientCase;
use PDF;
use Mail;
use App\User;


class TestHealthFacilityController extends Controller
{
	   public function index () {

        $health_facilities = HealthFacility::all();

        return view('test_health_facilities.index', compact('health_facilities'));
    }
    
    public function create(){
        return view('test_health_facility.new');
    }

    public function store(Request $request){
        
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'location' => 'required',
            'district' => 'required'
        ]);

        if($validator->fails()){
            return redirect()->back();
        }

        $health_facility = new HealthFacility;

        $health_facility->name = $request->name;
        $health_facility->location = $request->location;
        $health_facility->district = $request->district;

        $health_facility->save();

        Session::flash('success', 'Health Facility Added Successfully');

        return redirect()->route('index_health_facility');

    }

 
}

