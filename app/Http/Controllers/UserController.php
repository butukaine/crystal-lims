<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Notification;
use App\Notifications\UserActivation;
use Mail;

class UserController extends Controller
{

    public function index_clinicians(){
        Session(['title'=>'Clinicians']);
    	$clinicians = User::where('role', 'Clinician')->get();
    	return view('users.index')->with(['users' => $clinicians]);
    }

    public function index_hfusers(){
        Session(['title'=>'Health Facility Users']);
    	$hfusers = User::where('role', 'Health Facility User')->get();
    	return view('users.index')->with(['users' => $hfusers]);
    }

    public function index_academicians(){
        Session(['title'=>'Clinicians']);
    	$academicians = User::where('role', 'Academician')->get();
    	return view('users.index')->with(['users' => $academicians]);
    }
   public function index_admin(){
            Session(['title'=>'Low Level Administrators']);

        $admin = User::where('role', 'Low Level Admin')->get();
        return view('users.index')->with(['users' => $admin]);
    }
    public function profile(){
        $user = Auth::user();
        if($user->role == "Academician"){
            return view('users.profile_academician')->with(['user'=> $user]);

        }else{
        	return view('users.profile')->with(['user'=> $user]);
        }
    }

    public function activate(Request $request){
    	$user = User::find($request->user_id);
    	$user->account_state = 1;
    	$user->save();

        $to_name = $user->name;
        $to_email = $user->email;
        $data = array('name'=> $to_name, "body" => "Test mail");
        
        Mail::send('emails.user_activation', $data, function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                    ->subject('User Account Activated');
            $message->from('abanegura@gmail.com','MBC');
        });

    	return redirect()->back();
    }

       public function deactivate(Request $request){
    	$user = User::find($request->user_id);
    	$user->account_state = 0;
    	$user->save();

    	return redirect()->back();
    }
    
   
 
}
