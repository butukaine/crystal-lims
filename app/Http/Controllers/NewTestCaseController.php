<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Submission;
use App\Response;
use Sentinel;
use Session;
use Image;
use App\PatientRequest;
use App\Report;
use Carbon\Carbon;
use App\PatientCase;
use PDF;
use Mail;
use App\User;

class NewTestCaseController extends Controller
{

	public function create()
    {
    	Session(['title'=>'Create Case']);
    	$case = new PatientCase;
    	return view('test_cases.new_test_case')->with(['case'=>$case]);
    }

    public function store(Request $request){
    	if($request->id){
            $case = PatientCase::find($request->id);

        }else{
            $case = new PatientCase();
        }


        $case->patient_name = $request->patient_name;
        $case->phys_surg = $request->phys_surg;
        $case->age = $request->case;
        $case->gender = 1;//\$request->case;
        $case->patient_number = $request->patient_number;//$request->sex;
        $case->village = $request->village;
        $case->parish = $request->parish;
        $case->sub_county = $request->sub_county;
        $case->district = $request->district;
       	$case->specimen = $request->specimen;
       	$case->date_of_request =$request->date_of_request;
       	$case->clinical_summary = $request->clinical_summary;
       	$case->nationality = $request->nationality;
       	$case->health_facility = $request->health_facility;


       	$case->save();
        return redirect('test_cases');
}
}
