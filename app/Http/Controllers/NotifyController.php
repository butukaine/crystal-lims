<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\PatientRequest;
use Notification;
use App\Notifications\UserActivation;
use App\Notifications\NewUserRegister;
use App\Notifications\ReportAvailable;
use Illuminate\Notifications\Notifiable;
use Mail;
class NotifyController extends Controller
{
  public function __construct()
  {
    // $this->middleware('auth');
  }
  
  public function index()
  {
  	$user = User::where('email', 'bensek73@gmail.com')->first();
  	$user2 = User::where('email', 'ben@kolastudios.com')->first();
  	$to_name = $user->name;
	$to_email = $user->email;
	$data = array('name'=> $to_name, "body" => "Test mail");
    
	Mail::send('emails.user_activation', $data, function($message) use ($to_name, $to_email, $user2) {
	    $message->to($to_email, $to_name)
	    		->cc($user2->email, $user2->name)
	            ->subject('User Account Activated');
	    $message->from('mbcug20@gmail.com','MBC');
	});
   
  } 

  public function new_user(){

  	$user = User::where('email', 'bensek73@gmail.com')->first();

  	$to_name = "Administrator";
	$to_email = "mbcug20@gmail.com";
	$data = array('name'=> $user->name);
    
	Mail::send('emails.new_user', $data, function($message) use ($to_name, $to_email, $user) {
	    $message->to($to_email, $to_name)
	            ->subject('New User Has Registered');
	    $message->from($user->email,$user->name);
	});
  }

  public function report_available(){
  	 $req = PatientRequest::find(10);
  	 //$user = $req->clinician;
  	 $user = User::where('email', 'bensek73@gmail.com')->first();

  	$to_name = $user->name;
	$to_email = $user->email;
	$data = array('name'=> $to_name, "body" => "Test mail");
    
	Mail::send('emails.report_available', $data, function($message) use ($to_name, $to_email) {
	    $message->to($to_email, $to_name)
	            ->subject('Report Available');
	    $message->from('mbcug20@gmail.com','MBC');
	});
  	 $user->notify(new ReportAvailable);

  }
}