<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Searchable\Search;
use App\PatientCase;
use App\HealthFacility;
use Auth;
use Session;
use DateTime;
use App\SearchResult;
use DB;

class SearchController extends Controller
{

	public function create(){
		return view('searches.new');
	}


       public function start()
    {
        return view('searches.newaca')->with(['health_units' => HealthFacility::all()]);
    }

    public function seeResults($id){
     	$search = \App\Search::find($id);
    	return view('searches.results')->with(['search'=> $search]);
    }

    public function index(){

        $searches = \App\Search::all(); //orderBy('searches.id', 'DESC')->get();
    	return view('searches.index')->with(['searches' => $searches]);
    }

    public function mysearches(){
    	return view('searches.mysearches')->with(['searches' => \App\Search::where('searched_by', Auth::id())->get()]);
    }
    public function store(Request $request){

    	$search = new \App\Search;
    	$search->specimen = $request->specimen;
    	$search->gender = $request->gender;
    	$search->searched_by = Auth::id();
    	$search->age_start = $request->age_start;
    	$search->age_end = $request->age_end;
    	$search->date_from = $request->date_from;
    	$search->date_to = $request->date_to;

    	$search->save();

    	return redirect()->route('index_search');
    }
 	public function storeAcademician(Request $request){

    	$search = new \App\Search;
    	$search->specimen = $request->specimen;
    	$search->gender = $request->gender;
    	$search->searched_by = Auth::id();
    	$search->age_start = $request->age_start;
    	$search->age_end = $request->age_end;
        $search->health_facility = $request->health_facility;
        $search->conclusion = $request->conclusion;
        if($request->date_from != ""){
            $date1 = DateTime::createFromFormat('Y-m-d', $request->date_from);
            $search->date_from = $date1->format('j, M Y');   
        }
        if($request->date_to != ""){
            $date2 = DateTime::createFromFormat('Y-m-d', $request->date_to);
            $search->date_to = $date2->format('j, M Y');    
        }

    	$search->save();
        Session::flash("success","Search request received. Wait for your results to be uploaded.");
    	return redirect()->back();
    }

    public function approve(Request $request){
    	$search = \App\Search::find($request->search_id);
    	
        $cases = PatientCase::where('specimen', $search->specimen)->get();
        foreach ($cases as $key => $value) {
            $result = new SearchResult;
            $result->academician = $search->user->name;
            $result->date_searched = $search->created_at->format('j, M Y');
            $result->search_id = $search->id;
            $result->case_id = $value->id;
            $result->response_id = $value->response_id;
            $result->biopsy_no = $value->response->biopsy_no;
            $result->save();
        }
        $search->approved = 1;
        $search->save();

    	return redirect()->route('index_search');
    }

}
  