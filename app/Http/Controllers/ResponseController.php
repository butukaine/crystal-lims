<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Submission;
use App\Response;
use Sentinel;
use Session;
use Image;
use App\PatientRequest;
use App\Report;
use Carbon\Carbon;
use App\PatientCase;
use PDF;
use Mail;
use App\User;

class ResponseController extends Controller
{
	public function index(){

		Session(['title'=>'Reports']);
		return view('responses.index')->with([
			'responses' => Report::all()
		]);
	}

	public function create($id){

		$request = PatientRequest::find($id);
		$case = $request->case;
		return view('responses.new')->with([
			'request' => $request,
			'case' => $case,
		]);
	}
	public function submit($id){

		$report = Report::find($id);
		$req = PatientRequest::find($report->request_id);
    	$req->responded = 1;
	    $req->response_id = $id;
	    $req->save();

	    if($req->save()){
	    	$report->submitted = 1; //submitted
	    	$report->submitted_at = Carbon::now(new \DateTimeZone('Africa/Kampala'))->format('D, j M Y');
			$report->save();
	    }
	   
    	$case = PatientCase::find($req->case->id);
    	if($case){
    		$case->response_id = $id;
    		$case->save();
    	}	    

	    //Get the user who submitted the request and send them a notification
	    $case_user = $req->clinician;

  		// $to_name = $case_user->name;
		// $to_email = $case_user->email;
		// $data = [];
	    
		// Mail::send('emails.report_available', $data, function($message) use ($to_name, $to_email) {
		//     $message->to($to_email, $to_name)
		//             ->subject('Report Available');
		//     $message->from('abanegura@gmail.com','MBC');
		// });

		Session::flash('success', 'Response submitted successfully');

		return redirect()->route('index_responses');
	}
	public function store(Request $request){
		$report = new Report;
		$report->clinical_summary = $request->clinical_summary;
	    $report->ma_appearance = $request->macroscopic;
		$report->mi_appearance = $request->microscopic;
		$report->conclusion = $request->conclusion;
		$report->comments = $request->comments;
		$report->health_unit = $request->health_unit;
		$report->physician_name = $request->physician_name;
		$report->physician_contact = $request->physician_contact;
		$report->physician_mobile = $request->physician_mobile;
		$report->date_of_receipt = Carbon::createFromFormat('Y-m-d', $request->date_of_receipt)->isoFormat('dddd, Do MMMM YYYY');
		$report->biopsy_no = $request->biopsy_no;
	    $report->request_id = (int) $request->request_id;
	    $report->pathologist_name = $request->pathologist_name;
	    $report->date_of_report = Carbon::createFromFormat('Y-m-d', $request->date_of_report)->isoFormat('dddd, Do MMMM YYYY');;
	    if($request->hasFile('report_doc')){	
		    $uniqueFileName = "REPORT_" . $request->biopsy_no ."_". time() . "." .  $request->file('report_doc')->getClientOriginalExtension();
	        //$request->file('report_doc')->move(public_path() . '/uploads/responses', $uniqueFileName);
	        $report->document = $request->report_doc->storeAs('/uploads/responses', $uniqueFileName, 'public');
	    }
		
		$req = PatientRequest::find($report->request_id);
    	$req->responded = 2; // saved
	    $req->response_id = $report->id;
	    $req->save();
	    $report->case_id = $req->case->id;
		$report->save();

		$case = $req->case;
    	$case->patient_name = $request->patient_name;
    	$case->age = $request->age;
    	$case->gender = $request->gender;
    	$case->patient_number = $request->patient_number;
    	$case->village = $request->village;
    	$case->parish = $request->parish;
    	$case->district = $request->district;
    	$case->sub_county = $request->sub_county;
    	$case->specimen = $request->specimen;
    	$case->clinical_summary = $request->clinical_summary;
    	$case->save();


		Session::flash('success', 'Response saved successfully');
		return redirect()->route('index_responses');

	}

	public function show($id){
		$response = Report::find($id);
		$request = PatientRequest::find($response->request_id);
		$case = $request->case;

		Session(['title'=> $response->biopsy_no]);
		return view('responses.show')->with([
			'response' => $response,
			'request' => $request,
			'case' => $case,
		]);
	}

	public function edit($id){
		$response = Report::find($id);
		$request = PatientRequest::find($response->request_id);
		$case = $request->case;
		
		return view('responses.edit')->with([
			'response' => $response,
			'request' => $request,
			'case' => $case,
		]);
	}

	public function update(Request $request, $id){
		$report = Report::find($id);
		$report->clinical_summary = $request->clinical_summary;
	    $report->ma_appearance = $request->macroscopic;
		$report->mi_appearance = $request->microscopic;
		$report->conclusion = $request->conclusion;
		$report->physician_name = $request->physician_name;
		$report->physician_contact = $request->physician_contact;
		$report->physician_mobile = $request->physician_mobile;
		$report->date_of_receipt = Carbon::createFromFormat('Y-m-d', $request->date_of_receipt)->isoFormat('dddd, Do MMMM YYYY');
		$report->biopsy_no = $request->biopsy_no;
	    $report->request_id = (int) $request->request_id;
	    if($request->hasFile('report_doc')){
		    $uniqueFileName = "Response_" . $request->file('report_doc')->getClientOriginalName();
	        $request->file('report_doc')->move(public_path() . '/uploads/responses', $uniqueFileName);
	        $report->document = $uniqueFileName;
	    }
		$report->save();
		if($report->save()){
			$req = PatientRequest::find($report->request_id);
	    	$req->responded = 1;
		    $req->response_id = $report->id;
		    $req->save();
		}
		Session::flash('success', 'Report Updated Successfully!');
		return redirect()->route('index_responses');
	}

	public function destroy($id)
    {
        $resp = Report::find($id);
        $resp->delete();

        return redirect('/responses')->with('success', 'Response has been deleted!');
    }

    public function downloadList(){
       $date = Carbon::now()->format('j, M Y');
       $responses = Report::all();

        $pdf = PDF::loadView('responses.response_list_pdf', compact('responses', 'date'));
        return $pdf->download('Responses('. $date .').pdf');
    }

    public function downloadResponse($id){
        $response = Report::find($id);
        $request = PatientRequest::find($response->request_id);
		$case = $request->case;
        $pdf = PDF::loadView('responses.response_pdf', compact('response', 'request', 'case'));
            
        return $pdf->download('Report#'. $response->id . '.pdf');
    }

	public function detail_submission($id){

		$submission_detail = Submission::findOrFail($id);

		// dd($submission_detail);
		
		// $submission_detail_more = DB::table('users')
		// ->leftjoin('submissions', 'users.id', '=', 'submissions.user_id')
		// ->join('responses', 'submissions.id', '=', 'responses.submissions_id')
		// ->select('users.*', 'submissions.*', 'responses.*')
		// ->first();

		// $submission_detail_more = $submission_detail->response();

		// dd($submission_detail);

		return view('admin.submissions.submission_detail', compact('submission_detail'));

	}

	public function getAllResponse(){

		$responses = DB::table('users')
		->join('submissions', 'users.id', '=', 'submissions.user_id')
		->join('responses', 'submissions.id', '=', 'responses.submissions_id')
		->where('users.id', Sentinel::getUser()->id)
		->select('users.*', 'submissions.*', 'responses.*')
		->get();

		// dd($responses);

		return view('clinicans.clinican_response', compact('responses'));

	}

	public function responseDetail ($id){

		$response_detail = DB::table('users')
		->join('submissions', 'users.id', '=', 'submissions.user_id')
		->join('responses', 'submissions.id', '=', 'responses.submissions_id')
		->where('users.id', Sentinel::getUser()->id)
		->where('submissions.id', $id)
		->select('users.*', 'submissions.*', 'responses.*')
		->first();

		// dd(asset('uploads/reports/'. $response_detail->report));

		return view('clinicans.clinican_response_detail', compact('response_detail'));

	}

	// public function viewReport ($report){

	// 	$report = DB::table('users')
	// 	->join('submissions', 'users.id', '=', 'submissions.user_id')
	// 	->join('responses', 'submissions.id', '=', 'responses.submissions_id')
	// 	->where('users.id', Sentinel::getUser()->id)
	// 	->where('submissions.id', $report)
	// 	->select('users.*', 'submissions.*', 'responses.*')
	// 	->first();


	// 	// dd($report->report);
	// 	$headers = ['Content-Type: application/pdf'];

	// 	return response()->download("uploads/reports/" . $report->report, $report->report, $headers);

	// 	// return response()->file('/uploads/reports/' . $report->report);

	// }
	

	public function send_report(Request $request){

		// dd($request->all());

		$image= $request->file('report');

		$filename = time(). '.' .$image->getClientOriginalExtension();

		$image->move('uploads/reports/', $filename);

		$report = new Response;

		$report->submissions_id = $request->submission_id;
		$report->report = $filename;

		$report->save();

		Session::flash('success', 'Report Submitted Successfully');

		return redirect()->back();

	}
}
