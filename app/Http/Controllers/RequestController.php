<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PatientRequest;
use App\PatientCase;
use Auth;
use Carbon\Carbon;
use PDF;
class RequestController extends Controller
{
	public function index(){

        Session(['title'=>'Requests']);
		return view('requests.index')->with([
			'requests' => PatientRequest::all()
		]);
	}
    public function store(Request $request){

    	$req = new PatientRequest;
    	$req->case_id = $request->case_id;
    	$req->created_by = Auth::id();
    	$req->date_submitted = Carbon::now(new \DateTimeZone('Africa/Kampala'))->isoFormat('dddd, Do MMMM YYYY');

    	$case = PatientCase::find($request->case_id);
    	$case->submitted = true;
    	$case->save();
    	if($case->save()){
    		$req->save();
    	}

    	return redirect()->back();
    }


    public function destroy($id)
    {
        $req = PatientRequest::find($id);
        $case = PatientCase::where('id', $req->case_id)->first();
        $case->submitted = false;
        $case->save();
        $req->delete();
        return redirect('/requests')->with('success', 'Request has been deleted!');
    }

    public function downloadList(){
       $date = Carbon::now()->format('j, M Y');
       $requests = PatientRequest::all();
        $pdf = PDF::loadView('requests.requests_pdf', compact('requests', 'date'));
        return $pdf->download('Requests('. $date .').pdf');
    }

}
