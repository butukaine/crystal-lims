<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SearchResult;
use Carbon\Carbon;
use App\SResult;
use Session;
class SearchResultsController extends Controller
{
    
    public function viewResultsTable(){
        return view('results.index')->with(['results' => SearchResult::all()]);
    }

    public function approveSearch(Request $request){
        $search = \App\Search::find($request->search_id);
        if($search->results_count != 0){
            $search->approved = 1;
            $search->save();
            Session::flash('success', 'Search has been approved!');
        }else{
            Session::flash('danger', 'Search has not been approved. Please add results before approving!');
        }

        return redirect()->route('index_search');
    }

    public function addResults($id){
        $search = \App\Search::find($id);

        return view('results.new')->with(['search' => $search]);

    }
    public function storeResult(Request $request){
    	$search  = \App\Search::find($request->search_id);
        $search->results_count = $search->results_count + 1;
        $search->save();
        $result = new SResult;
        $result->number = $search->results_count;
        $result->search_id = $request->search_id;
        $result->biopsy_no = $request->biopsy_no;
        $result->comments = $request->comments;
        $result->date_uploaded = Carbon::now(new \DateTimeZone('Africa/Kampala'))->format('D, j M Y');
        if($request->hasFile('file')){
            $uniqueFileName = "SearchID_" . $search->id . "_Result#" . $result->number . "." . $request->file('file')->getClientOriginalExtension();

            //$request->file('file')->move(public_path() . '/uploads/searched_files', $uniqueFileName);
            $result->file = $request->file->storeAs('/uploads/searched_files', $uniqueFileName, 'public');
        }
    	$result->save();
        Session::flash('success', 'Your result has been uploaded successfully!');
    	return redirect()->back();
    }

    public function getResults($id){
        $search = \App\Search::find($id);
        $results = SResult::where('search_id', $search->id)->get();

        return view('searches.results')->with(['results'=> $results]);
    }

    public function getAcademicianResults($id){
        $search = \App\Search::find($id);
        $results = SResult::where('search_id', $search->id)->get();

        return view('results.myresults')->with(['results'=> $results]);
    }

}
