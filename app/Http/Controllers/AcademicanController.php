<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Sentinel;
use Session;
use App\Patient;
use App\Treatment;
use App\HealthFacility;

class AcademicanController extends Controller
{
	public function create() {

		return view('admin.academicans.create');

	}

	public function store(Request $request){

		$validator = Validator::make($request->all(), [
			'email'    => 'required|email|unique:users,email',
			'first_name' => 'required',
			'last_name' => 'required',
			'password' => 'required',
			'address' => 'required',
			'telephone_number' => 'required'
		]);

		if ($validator->fails()) {

			Session::flash('error', 'Please Check your Inputs');

			return redirect()->back();
		}

		$credentials = [
			'email'    => $request->email,
			'first_name' => $request->first_name,
			'last_name' => $request->last_name,
			'password' => $request->password,
			'address' => $request->address,
			'telephone_number' => $request->telephone_number,
		];

		$user = Sentinel::registerAndActivate($credentials);

		$role = Sentinel::findRoleBySlug("academican");

		$role->users()->attach($user);

		Session::flash('success', 'A New Academican Added Successful.');

		return redirect()->route('academicans.index');
	}

	public function index() {

		$academicans = Sentinel::findRoleBySlug('academican')->users()->with('roles')->get();

		return view('admin.academicans.index', compact('academicans'));
		
	}

	public function academicanDashboard() {

		$health_facilities_count = HealthFacility::count();

		$clinicans_count = Sentinel::findRoleBySlug('clinican')->users()->with('roles')->count();

		$patients_count = Patient::count();

		$treatments_count = Treatment::count();

		$first_pie_chart = [
			'groups' => ['Facilities', 'Pathology Cases'],
			'data' => [$health_facilities_count, $clinicans_count]
		];

		$second_pie_chart = [
			'groups' =>['Pathology Cases', 'Age'],
			'data' => [$patients_count, $treatments_count]
		];

		return view('academicans.academican_dashboard', compact('first_pie_chart', 'second_pie_chart'));

	}
}
