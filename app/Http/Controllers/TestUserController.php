<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Notification;
use App\Notifications\UserActivation;
use Mail;

class TestUserController extends Controller
{

    public function index_clinicians(){
    	$clinicians = User::where('role', 'Clinician')->get();
    	return view('test_users.clinicians')->with(['clinicians' => $clinicians]);
    }

    public function index_hfusers(){
    	$hfusers = User::where('role', 'Health Facility User')->get();
    	return view('test_users.hfusers')->with(['hfusers' => $hfusers]);
    }

    public function index_academicians(){
    	$academicians = User::where('role', 'Academician')->get();
    	return view('test_users.academicians')->with(['academicians' => $academicians]);
    }
   public function index_admin(){
        $admin = User::where('role', 'Low Level Admin')->get();
        return view('test_users.admin')->with(['admin' => $admin]);
    }
    public function profile(){
        $user = Auth::user();
        if($user->role == "Academician"){
            return view('test_users.profile_academician')->with(['user'=> $user]);

        }else{
        	return view('test_users.profile')->with(['user'=> $user]);
        }
    }

    public function activate(Request $request){
    	$user = User::find($request->user_id);
    	$user->account_state = 1;
    	$user->save();

        $to_name = $user->name;
        $to_email = $user->email;
        $data = array('name'=> $to_name, "body" => "Test mail");
        
        Mail::send('emails.user_activation', $data, function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                    ->subject('User Account Activated');
            $message->from('abanegura@gmail.com','MBC');
        });

    	return redirect()->back();
    }

       public function deactivate(Request $request){
    	$user = User::find($request->user_id);
    	$user->account_state = 0;
    	$user->save();

    	return redirect()->back();
    }
    
   
 
}
