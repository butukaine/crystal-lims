<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PatientCase;
use App\PatientRequest;
use App\HealthFacility;
use PDF;
use Auth;
use Session;
use Carbon\Carbon;
class CaseController extends Controller
{
	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
    	Session(['title' => 'Cases']);
        $role = Auth::user()->role;
        if($role == "admin"){
            return view('cases.index')->with(['cases' => PatientCase::orderBy('id', 'desc')->get()]);
        }else if($role == "Clinician" || $role == "Low Level Admin"){
            $cases = PatientCase::where('created_by', Auth::id())->get();
            return view('cases.index')->with(['cases' => $cases]);
        }else if($role == "Health Facility User"){
            $cases = PatientCase::where('created_by', Auth::id())->get();
            return view('cases.hfusers_index')->with(['cases' => $cases]);
        }

    }

     public function create(){
    	session(['title' => 'New Case']);
        $health_units = HealthFacility::all();
        if(Auth::user()->role == "Health Facility User"){
            return view('cases.uploadcase');
        }else{
            return view('cases.new');
        }

    	
    }
    public function storeDoc(Request $request){
         $this->validate($request,[
            'case_doc' => 'required',
        ]);
        $case = new PatientCase;
        $case->created_by = Auth::id();
        $case->code = "CASE_" . time();
        $case->patient_name = "";
        $case->phys_surg = "";
        $case->age = "";
        $case->gender = "";
        $case->patient_number = "";
        $case->health_facility = "";
        $case->other_facility = "";
        $case->nationality = "";
        $case->village = "";
        $case->parish = "";
        $case->district = "";
        $case->sub_county = "";
        $case->specimen = "";
        $case->date_of_request = "";
        $case->clinical_summary = "";
        if($request->hasFile('case_doc')){
            //$uniqueFileName = "Case_" . $request->file('case_doc')->getClientOriginalName();
            $uniqueFileName = $case->code . "_File." . $request->file('case_doc')->getClientOriginalExtension();
            // $request->file('case_doc')->move(public_path() . '/uploads/cases', $uniqueFileName);
            // $case->document = $uniqueFileName;
            $case->document = $request->case_doc->storeAs('/uploads/cases', $uniqueFileName, 'public');
        }
        $case->save();
        return redirect('/cases');
    }
    public function store(Request $request){

    	$case = new PatientCase;
        //$case->created_by = Auth::id();
        $case->code = "CASE_".time(); 
    	$case->patient_name = $request->patient_name;
    	$case->phys_surg = $request->phys_surg;
    	$case->age = $request->age;
    	$case->gender = $request->gender;
    	$case->patient_number = $request->patient_number;
    	$case->health_facility = $request->health_facility;
        $case->nationality = $request->nationality;
    	$case->village = $request->village;
    	$case->parish = $request->parish;
    	$case->district = $request->district;
    	$case->sub_county = $request->sub_county;
    	$case->specimen = $request->specimen;
    	$case->date_of_request = Carbon::createFromFormat('Y-m-d', $request->date_of_request)->isoFormat('dddd, Do MMMM YYYY');
    	$case->clinical_summary = $request->clinical_summary;
        if($request->hasFile('case_doc')){
            //$uniqueFileName = "Case_" . $request->file('case_doc')->getClientOriginalName();
            $uniqueFileName = $case->code . "_File." . $request->file('case_doc')->getClientOriginalExtension();
            // $request->file('case_doc')->move(public_path() . '/uploads/cases', $uniqueFileName);
            // $case->document = $uniqueFileName;
            $case->document = $request->case_doc->storeAs('/uploads/cases', $uniqueFileName, 'public');
        }
        
    	$case->save();
    	return redirect()->route('index_cases');
    }
    public function show($id){
        $case = PatientCase::find($id);
        Session(['title'=> $case->code]);
        return view('cases.show')->with(['case'=> $case]);
     
      }
    public function edit($id){
        $case = PatientCase::find($id);
                Session(['title'=> "Edit Case"]);
        return view('cases.edit')->with(['case' => $case]);
    }

    public function update(Request $request, $id){
        $case = PatientCase::find($id);
        $case->patient_name = $request->patient_name;
        $case->phys_surg = $request->phys_surg;
        $case->age = $request->age;
        $case->gender = $request->gender;
        $case->patient_number = $request->patient_number;
        $case->health_facility = $request->health_facility;
        $case->nationality = $request->nationality;
        $case->village = $request->village;
        $case->parish = $request->parish;
        $case->district = $request->district;
        $case->sub_county = $request->sub_county;
        $case->specimen = $request->specimen;
        $case->date_of_request = Carbon::createFromFormat('Y-m-d', $request->date_of_request)->isoFormat('dddd, Do MMMM YYYY');
        $case->clinical_summary = $request->clinical_summary;
        if($request->hasFile('case_doc')){
            //$uniqueFileName = "Case_" . $request->file('case_doc')->getClientOriginalName();
            $uniqueFileName = $case->code . "_File." . $request->file('case_doc')->getClientOriginalExtension();
            $request->file('case_doc')->move(public_path() . '/uploads/cases', $uniqueFileName);
            $case->document = $uniqueFileName;
        }
        $case->save();
        return redirect('/cases')->with('success', 'Case has been updated!');
    }
    public function downloadPDF($id) {
        $case = PatientCase::find($id);
        $health_unit = HealthFacility::find($case->health_facility);
        $pdf = PDF::loadView('cases.cases_list_pdf', compact('show'));
            
            return $pdf->download('disney.pdf');
    }
    public function downloadList(){
        $date = Carbon::now()->format('j, M Y');
       $role = Auth::user()->role;

        if($role == "admin"){
            $cases = PatientCase::all();
        }else if($role == "Clinician" || $role == "Low Level Admin"){
            $cases = PatientCase::where('created_by', Auth::id())->get();
        }else if($role == "Health Facility User"){
            $cases = PatientCase::where('created_by', Auth::id())->get();
        }

        $pdf = PDF::loadView('cases.cases_list_pdf', compact('cases', 'date'));
        return $pdf->download('Cases('. $date .').pdf');
    }

    public function downloadCase($id){
        $case = PatientCase::find($id);
        $health_unit = HealthFacility::find($case->health_facility);
        $pdf = PDF::loadView('cases.case_pdf', compact('case', 'health_unit'));
            
        return $pdf->download($case->code . '.pdf');
    }

   
    public function destroy($id)
    {
        $case = PatientCase::find($id);
        $req = PatientRequest::where('case_id', $case->id)->first();
        if($req){
            $req->delete();
        }
        $case->delete();
        return redirect('/cases')->with('success', 'Case has been deleted!');
    }
    function generateBarcodeNumber() {
    $number = mt_rand(1000000000, 9999999999); // better than rand()

    // call the same function if the barcode exists already
    if (barcodeNumberExists($number)) {
        return generateBarcodeNumber();
    }

    // otherwise, it's valid and can be used
    return $number;
    }

function barcodeNumberExists($number) {
    // query the database and return a boolean
    // for instance, it might look like this in Laravel
    return User::whereBarcodeNumber($number)->exists();
}
}
