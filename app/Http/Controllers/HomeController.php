<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\PatientCase;
use App\PatientRequest;
use App\Report;
use App\HealthFacility;
use Auth;
use App\User;
use DB;
use Carbon\Carbon;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        if(Auth::user()->role == "Academician"){
            return redirect()->route('start_search');
        }

        if(Auth::user()->role == "Clinician" || Auth::user()->role == "Health Facility User"){
            return redirect()->route('index_cases');
        }
        if(Auth::user()->role == "Low Level Admin"){
            return redirect()->route('index_requests');
        }

        if(Auth::user()->role == "admin"){
            $cases = PatientCase::count();
            $requests = PatientRequest::count();
            $responses = Report::count();
            $users = User::count();
            
            Session(['title' => 'Dashboard']);

            return view('home')->with([
                'cases' => $cases,
                'requests' => $requests,
                'responses' => $responses,
                'users' => $users,
            ]);
        }
    }

    public function date(){
        $dt = Carbon::createFromFormat('Y-m-d', '2020-02-09')->isoFormat('dddd, Do MMMM YYYY');
        print_r(json_encode($dt));
    }

    public function getStats(){
        $monthly_cases = DB::table('cases')->select(DB::raw('COUNT(id) as total'), DB::raw('MONTH(created_at) as month'))
                    ->groupBy('month')
                    ->get();
        $cases = [0,0,0,0,0,0,0,0,0,0,0,0];//initialize all months to 0

        foreach($monthly_cases as $key){
           $cases[$key->month-1] = $key->total;//update each month with the total value
        }

        $monthly_responses = DB::table('reports')->select(DB::raw('COUNT(id) as total'), DB::raw('MONTH(created_at) as month'))->groupBy('month')
               ->get();
        $responses = [0,0,0,0,0,0,0,0,0,0,0,0];//initialize all months to 0
        foreach($monthly_responses as $key){
           $responses[$key->month-1] = $key->total;//update each month with the total value
        }
        return response()->json(array(
            'cases' => $cases,
            'responses' => $responses,
        ));
    }
    public function download(Request $request){
        return Storage::disk('public')->download($request->path);
    }
     public function displayFile($path){
        return response()->file(public_path() . $path);
        //return Storage::disk('local_public')->download($path);
    }
    public function logout(){
        Auth::logout();
        return redirect('/login');
    }
}
