<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HealthFacility;
use Validator;
use Session;

class HealthFacilityController extends Controller
{
    public function create(){
        return view('facility.new');
    }

    public function store(Request $request){
        
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'location' => 'required',
            'district' => 'required'
        ]);

        if($validator->fails()){
            return redirect()->back();
        }

        $health_facility = new HealthFacility;

        $health_facility->name = $request->name;
        $health_facility->location = $request->location;
        $health_facility->district = $request->district;

        $health_facility->save();

        Session::flash('success', 'Health Facility Added Successfully');

        return redirect()->route('index_health_facility');

    }

    public function index () {

        $health_facilities = HealthFacility::all();

        return view('facility.index', compact('health_facilities'));
    }
}
