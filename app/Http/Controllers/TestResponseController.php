<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Submission;
use App\Response;
use Sentinel;
use Session;
use Image;
use App\PatientRequest;
use App\Report;
use Carbon\Carbon;
use App\PatientCase;
use PDF;
use Mail;
use App\User;

class TestResponseController extends Controller
{
	public function test_response(){
		return view('new_responses.new_responses')->with([
			'responses' => Report::all()
		]);
	}
}
