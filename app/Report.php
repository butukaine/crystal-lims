<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
class Report extends Model implements HasMedia
{
	use HasMediaTrait;
    public $table = 'reports';

    public function request(){
    	return $this->belongsTo('App\PatientRequest', 'request_id');
    }

    public function case(){
    	return $this->hasOne('App\PatientCase', 'case_id');
    }
}
