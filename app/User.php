<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;

class User extends Authenticatable
{
    use Notifiable, CanResetPassword;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function submissions(){

        return $this->hasMany(Submission::class);
        
    }

    public function requests(){
        return $this->hasMany('App\PatientRequest');
    }

    public function searches(){
        return $this->hasMany('App\Search');
    }

    public function healthFacility(){
        return $this->belongsTo('App\HealthFacility', 'health_facility_id');
    }
    public function setPasswordAttribute($password)
                {
                $this->attributes['password'] = bcrypt($password);
                }


}

//'role', 'photo', 'address', 'sex', 'dob', 'nin', 'nationality', 'phone', 'health_facility_id'