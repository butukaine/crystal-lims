<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HealthFacility extends Model
{
	public function cases(){
		return $this->hasMany('App\PatientCase');
	}

	public function users(){
		return $this->hasMany('App\User');
	}
}
