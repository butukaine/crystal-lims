<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserActivation extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->from('testemail@gmail.com', 'Sender')
                    ->subject('MBC Account Activation')
                    ->greeting('Hello!')
                    ->line('Your request to login to https://mbcug.com/ has been approved. You can now start using the system.')
                    ->line('You can also check out our user manual for detailed instructions on how to use the system.')
                    ->action('Get Started', url('https://docs.google.com/presentation/u/1/d/1br6hf19uuiHhGcjNk7cpsQjVQA8OFy0WTVU6ODH4xZ8/edit?usp=sharing'))
                    ->line('Thank you!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
