<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
class PatientCase extends Model implements HasMedia, Searchable
{
	use HasMediaTrait;

    protected $table = 'cases';

    public function response(){
    	return $this->hasOne('App\Report', 'case_id');
    }

    public function healthUnit(){
        return $this->belongsTo('App\HealthFacility');
    }

    public function getSearchResult(): SearchResult
    {
        //$url = route('categories.show', $this->id);
    	$url = route('start_search');
        return new SearchResult(
            $this,
            $this->specimen,
            $url
         );
    }
}
