<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Case extends Model implements HasMedia
{
	use HasMediaTrait;
    public $table = 'cases';

    public function request(){
    	return $this->hasOne('App\PatientRequest');
    }

    public function healthFacility(){
    	return $this->belongsTo('App\HealthFacility', 'health_facility');
    }
}
