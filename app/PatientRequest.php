<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatientRequest extends Model
{
    public $table = 'requests';

    public function case(){
    	return $this->belongsTo('App\PatientCase', 'case_id');
    }

    public function clinician(){
    	return $this->belongsTo('App\User', 'created_by');
    }
}
