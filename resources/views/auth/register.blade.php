@extends('auth.basic')

@section('content')
 <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-wrapper-before"></div>
            <div class="content-header row">
            </div>
            <div class="content-body" id="login-bg">
                <section class="flexbox-container">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="col-lg-6 col-md-6 col-10 box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                                <div class="card-header border-0">
                                    <!-- <div class="text-center mb-1">
                                        <img src="/images/logo/logo.png" alt="branding logo" id="logo-small">
                                    </div> -->
                                    <div class="font-large-1  text-center text-primary">
                                        Registration
                                    </div>
                                </div>
                                <div class="card-content">

                                    <div class="card-body">
                                        <form class="form-horizontal" method="POST" action="{{route('register')}}">
                                            @csrf
                                            <input type="hidden" name="role" value="3">
                                            <div class="row">
                                                <div class="col-4">
                                                    <label>Full Name<span class="text-danger">*</span></label>

                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="text" class="form-control" id="user-name" placeholder="e.g. Mugisha Andrew"  autocomplete="off" name="name" required="">
                                                        <div class="form-control-position">
                                                            <i class="ft-user"></i>
                                                        </div>
                                                         @error('name')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>
                                                </div>
                                                
                                                <div class="col-4">
                                                    <label>Phone Number</label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="text" class="form-control" id="user-name" placeholder="e.g. 0781234567" autocomplete="off" name="phone">
                                                        <div class="form-control-position">
                                                            <i class="ft-user"></i>
                                                        </div>

                                                    </fieldset>
                                                </div>
                                                <div class="col-4">
                                                     <label>Physical Address</label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="text" class="form-control" id="user-email" placeholder="e.g. Ntinda, Kampala"  name="address">
                                                        <div class="form-control-position">
                                                            <i class="ft-mail"></i>
                                                        </div>
                                                    </fieldset> 
                                                </div>
                                                
                                            </div>

                                            <div class="row">
                                                <div class="col-4">
                                                    <label>Nationality</label>

                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="text" class="form-control" placeholder="e.g. Ugandan"  autocomplete="off" name="nationality">
                                                        <div class="form-control-position">
                                                            <i class="ft-user"></i>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                                <div class="col-4">
                                                     <label>NIN/Passport Number</label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="text" class="form-control" id="user-email" placeholder="e.g. CM999955566YT34"  name="nin">
                                                        <div class="form-control-position">
                                                            <i class="ft-mail"></i>
                                                        </div>
                                                    </fieldset> 
                                                </div>
                                                <div class="col-4">
                                                    <label>Date of Birth</label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="date" class="form-control" id="user-name" autocomplete="off" name="dob">
                                                        <div class="form-control-position">
                                                            <i class="ft-user"></i>
                                                        </div>

                                                    </fieldset>
                                                </div>
                                                
                                            </div>

                                            <div class="row">
                                                <div class="col-4">
                                                     <label>Email Address<span class="text-danger">*</span></label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="email" class="form-control" id="user-email" placeholder="e.g. andrew@gmail.com"  name="email" required="">
                                                        <div class="form-control-position">
                                                            <i class="ft-mail"></i>
                                                        </div>
                                                        @error('email')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>  
                                                </div>
                                                <div class="col-4">
                                                    <label>Password<span class="text-danger">*</span></label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="password" class="form-control" id="user-name" placeholder="Must be atleast 8 characters"  autocomplete="off" name="password" required="">
                                                        <div class="form-control-position">
                                                            <i class="ft-lock"></i>
                                                        </div>
                                                        @error('password')
                                                            <span id="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>
                                                </div>
                                                <div class="col-4">
                                                    <label>Confirm Password<span class="text-danger">*</span></label>
                                                     <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="password" class="form-control" id="user-name" placeholder="Re-enter your password"  name="password_confirmation" required="">
                                                        <div class="form-control-position">
                                                            <i class="ft-lock"></i>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                             <div class="row">
                            <div class="form-group col-9">
                            <label class="col-form-label">User Role<span class="text-danger">*</span></label><br>
                               <!--  <label class="radio-inline">
                                    <input type="radio" name="role" value="Academician">
                                    Academician
                                    <span></span>
                                </label> -->
                                <label class="radio-inline">
                                    <input type="radio" name="role" value="Clinician" required>
                                    Clinician
                                    <span></span>
                                </label>
                                
                                <label class="radio-inline">
                                    <input type="radio" name="role" value="Low Level Admin">
                                    Low Level Admin
                                    <span></span>
                                </label>
                                @if ($errors->has('role'))
                                    <span class="invalid-feedback text-danger" role="alert">
                                        {{ $errors->first('role') }}
                                    </span>
                                @endif
                        </div>
                        <div class="form-group col-lg-3">
                                <label for="sex" class=" col-form-label ">{{ __('Sex') }}<span class="text-danger">*</span></label><br>
                                <label class="radio-inline"><input name="sex" required value="Male" type="radio">Male</label> 
                                <label class="radio-inline"><input name="sex" value="Female" type="radio">Female</label>

                                @error('phone')
                                    <span class="invalid-feedback text-danger" role="alert">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group{{ $errors->has('terms') ? ' has-error' : '' }} col-lg-12">
                          
                            <input type="checkbox" name="terms" value="1" />
                            <label> I have read and agree to the <a href="{{ route('terms') }}">terms and conditions</a>
                            </label>

                          <div class="col-md-4">
                            @if ($errors->has('terms'))
                             <span class="help-block text-danger">
                               {{ $errors->first('terms') }}
                             </span>
                            @endif
                          </div>
                        </div>
                        </div>
                    </div>
                                            <div class="form-group text-center">
                                                <button type="submit" class="btn round btn-block btn-bg-gradient-x-purple-blue col-12 mr-1 mb-1 mt-2">Register</button>
                                            </div>

                                        </form>
                                    </div>
                                    <p class="card-subtitle text-muted text-center font-small-3 mx-1 my-1">
                                        <span>Already a member ?
                                            <a href="/login" class="card-link">Login</a>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <!-- END: Content-->

@endsection