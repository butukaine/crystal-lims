<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Register | {{ config('app.name')}}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link rel="stylesheet" href="/website/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="/website/css/animate.css">
    
    <link rel="stylesheet" href="/website/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/website/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="/website/css/magnific-popup.css">

    <link rel="stylesheet" href="/website/css/aos.css">

    <link rel="stylesheet" href="/website/css/ionicons.min.css">
    <link rel="stylesheet" href="/website/css/style.css">
      <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/vendors.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/components.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/pages/login-register.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
 
  </head>
<body class="vertical-layout vertical-menu 1-column  bg-full-screen-image blank-page blank-page" data-open="click" data-menu="vertical-menu" data-color="bg-gradient-x-purple-blue" data-col="1-column">
 
    <!-- END nav -->

    <section class="home-slider owl-carousel">

      <div class="slider-item" style="background-image: url(/website/images/bg_1.jpg);">
        <!-- <a href="" style="margin-top: 100px; margin-left: 100px">Back</a> -->

        <div class="overlay"></div>
        <div class="container-fluid">
          <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-wrapper-before"></div>
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="flexbox-container">
                    <div class="col-lg-12 d-flex align-items-center justify-content-center">
                        <div class="col-lg-8 box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                                <div class="card-header border-0">
                                <h3>REGISTER</h3>
                            
                                </div>
                                <div class="card-content">
                                     @if($errors->any())
                                    <div class="alert-example">
                                        <div class="alert alert-fill alert-danger alert-dismissible alert-icon">
                                            <em class="icon ni ni-cross-circle"></em>
                                            @foreach ($errors->all() as $error)
                                            {{$error}}
                                            @endforeach
                                            <button class="close" data-dismiss="alert"></button>
                                        </div>
                                        
                                    </div>
                                    @endif

                                    <div class="card-body">
                                        

                    <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data" autocomplete="off">
                        @csrf
                        <div class="row">
                            <div class="form-group col-lg-3">  
                                <label for="name" class="col-form-label">{{ ('Full Name*') }}</label>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                @error('name')
                                    <span class="invalid-feedback text-danger" role="alert">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group col-lg-3">
                                <label for="email" class=" col-form-label ">{{ __('E-mail*') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback text-danger" role="alert">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="phone" class=" col-form-label ">{{ __('Phone number*') }}</label>
                                <input id="phone" type="number" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" autocomplete="phone" placeholder="078xxxxx" required>

                                @error('phone')
                                    <span class="invalid-feedback text-danger" role="alert">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            

                            <div class="form-group col-lg-3">
                                <label for="nationality" class=" col-form-label ">{{ __('Nationality*') }}</label>
                                <input id="nationality" type="text" class="form-control @error('phone') is-invalid @enderror" name="nationality" value="{{ old('nationality') }}" autocomplete="phone" placeholder="Ugandan" required>

                                @error('phone')
                                    <span class="invalid-feedback text-danger" role="alert">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>
                             <div class="form-group col-lg-3">
                                <label for="nin" class=" col-form-label ">{{ __('NIN/Passport Number') }}</label>
                                <input id="nin" type="text" class="form-control @error('nin') is-invalid @enderror" name="nin" value="{{ old('nin') }}" autocomplete="phone">

                                @error('nin')
                                    <span class="invalid-feedback text-danger" role="alert">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="dob" class=" col-form-label ">{{ __('Date of Birth') }}</label>
                                <input id="dob" type="date" class="form-control @error('dob') is-invalid @enderror" name="dob" value="{{ old('dob') }}" autocomplete="dob">

                                @error('dob')
                                    <span class="invalid-feedback text-danger" role="alert">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">

                            <div class="form-group col-lg-3">
                                <label for="password" class=" col-form-label">{{ __('Password*') }}</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback text-danger" role="alert">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="password-confirm" class="col-form-label">{{ __('Repeat Password*') }}</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
                        
                        <div class="row">

                        <div class="form-group col-lg-3">  
                                <label for="address" class="col-form-label">{{ ('Physical Work Address*') }}</label>
                                <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" autocomplete="address" autofocus required>
                                @error('name')
                                    <span class="invalid-feedback text-danger" role="alert">
                                        {{ $message }}
                                    </span>
                                @enderror
                        </div>                       


                            <div class="form-group col-lg-3">  
                                <label for="photo" class="col-form-label">{{ ('Profile Photo') }}<small class="form_help"> (Must be jpeg or png)</small></label>
                                <input id="photo" type="file" class="form-control @error('photo') is-invalid @enderror" name="photo" value="{{ old('photo') }}" autocomplete="photo" autofocus accept="image/*">
                                @error('name')
                                    <span class="invalid-feedback text-danger" role="alert">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>
                            
                           
                             
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-9">
                            <label class="col-form-label">User Role*</label><br>
                                <label class="radio-inline">
                                    <input type="radio" name="role" value="Academician">
                                    Academician
                                    <span></span>
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="role" value="Clinician" required>
                                    Clinician
                                    <span></span>
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="role" value="Health Facility User">
                                    Health Facility User
                                    <span></span>
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="role" value="Low Level Admin">
                                    Low Level Admin
                                    <span></span>
                                </label>
                                @if ($errors->has('role'))
                                    <span class="invalid-feedback text-danger" role="alert">
                                        {{ $errors->first('role') }}
                                    </span>
                                @endif
                        </div>
                        <div class="form-group col-lg-3">
                                <label for="sex" class=" col-form-label ">{{ __('Sex*') }}</label><br>
                                <label class="radio-inline"><input name="sex" required value="Male" type="radio">Male</label> 
                                <label class="radio-inline"><input name="sex" value="Female" type="radio">Female</label>

                                @error('phone')
                                    <span class="invalid-feedback text-danger" role="alert">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('terms') ? ' has-error' : '' }} col-lg-12">
                          
                            <input type="checkbox" name="terms" value="1" />
                            <label> I have read and agree to the <a href="{{ route('terms') }}">terms and conditions</a>
                            </label>

                          <div class="col-md-4">
                            @if ($errors->has('terms'))
                             <span class="help-block text-danger">
                               {{ $errors->first('terms') }}
                             </span>
                            @endif
                          </div>
                        </div>
                    </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-warning">Register</button>
                            <a class="btn btn-primary" href="/login">Login</a>
                        </div>
                    </form>
                   </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
          
        </div>
      </div>

    
    </section>
    

    <footer class="ftco-footer ftco-bg-dark ftco-section">
      <div class="container">
       
        <div class="row">
          <div class="col-md-12 text-center">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This system is a product of <a href="http://crystaltechnologiesug.com/" target="_blank">Crystal Technologies</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
          </div>
        </div>
      </div>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="/website/js/jquery.min.js"></script>
  <script src="/website/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="/website/js/popper.min.js"></script>
  <script src="/website/js/bootstrap.min.js"></script>
  <script src="/website/js/jquery.easing.1.3.js"></script>
  <script src="/website/js/jquery.waypoints.min.js"></script>
  <script src="/website/js/jquery.stellar.min.js"></script>
  <script src="/website/js/owl.carousel.min.js"></script>
  <script src="/website/js/jquery.magnific-popup.min.js"></script>
  <script src="/website/js/aos.js"></script>
  <script src="/website/js/jquery.animateNumber.min.js"></script>
  <script src="/website/js/bootstrap-datepicker.js"></script>
  <script src="/website/js/jquery.timepicker.min.js"></script>
  <script src="/website/js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="/website/js/google-map.js"></script>
  <script src="/website/js/main.js"></script>
    <!-- BEGIN: Vendor JS-->
    <script src="app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="app-assets/vendors/js/forms/validation/jqBootstrapValidation.js" type="text/javascript"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="app-assets/js/core/app-menu.js" type="text/javascript"></script>
    <script src="app-assets/js/core/app.js" type="text/javascript"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="app-assets/js/scripts/forms/form-login-register.js" type="text/javascript"></script>
    <!-- END: Page JS-->


    
  </body>
</html>
