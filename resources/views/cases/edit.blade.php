@extends('layouts.master')
    
@section('content')
<div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">{{session('title')}}</h3>
                </div>
                <div class="content-header-right col-md-8 col-12">
                    <div class="breadcrumbs-top float-md-right">
                        <div class="breadcrumb-wrapper mr-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('index_cases')}}">Cases</a>
                                </li>
                                <li class="breadcrumb-item active">{{session('title')}}
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>



<div class="content-body">
    <!-- Zero configuration table -->
    <section id="configuration">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title text-primary">{{$case->code}}</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                           
                  <div class="row">
<!-- Form controls -->
                            <div class="col-sm-12 " data-lobipanel-child-inner-id="HmSnvsvMGa">
                                <div class="panel panel-bd " data-inner-id="HmSnvsvMGa" data-index="0">
                                    
                                    <div class="panel-body">
                                            @if ($errors->any())
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                             <form class="form form-horizontal" action="{{route('store_case')}}" method="POST" enctype="multipart/form-data" autocomplete="off">
                                                @csrf
                                                <div class="form-body">
                                                
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control" for="userinput1">PATIENT NAME<span class="text-danger">*</span></label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" placeholder="Patient Name" name="patient_name" required="" value="{{$case->patient_name}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control" for="userinput2">PATIENT NO</label>
                                                            <div class="col-md-9">
                                                                <input type="text" id="userinput2" class="form-control " placeholder="Patient Number" name="patient_number" value="{{$case->patient_number}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control" for="userinput3">NATIONALITY</label>
                                                            <div class="col-md-9">
                                                                <input type="text" id="userinput3" class="form-control " placeholder="Nationality" name="nationality" value="{{$case->nationality}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control" for="userinput4">AGE</label>
                                                            <div class="col-md-9">
                                                                <input type="text" id="userinput4" class="form-control " placeholder="Enter Age" name="age" value="{{$case->age}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control" for="userinput3">GENDER</label>
                                                           <div class="form-group">
                            
                            <div class="form-check form-check-inline" data-children-count="1">
                                <input class="form-check-input" type="radio" name="gender" id="gender1" value="Male">
                                <label class="form-check-label" for="gender1">Male</label>

                            </div>
                            <div class="form-check form-check-inline" data-children-count="1">
                                <input class="form-check-input" type="radio" name="gender" id="gender0" value="Female">
                                <label class="form-check-label" for="gender0">Female</label>
                            </div>
                        </div>
                                               
                                                
                                            
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control" for="userinput4">SUB-COUNTY</label>
                                                            <div class="col-md-9">
                                                                <input type="text" id="userinput4" class="form-control " placeholder="Sub County" name="sub_county" value="{{$case->sub_county}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control" for="userinput3">PARISH</label>
                                                            <div class="col-md-9">
                                                                <input type="text" id="userinput3" class="form-control " placeholder="Parish" name="parish" value="{{$case->parish}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control" for="userinput4">VILLAGE</label>
                                                            <div class="col-md-9">
                                                                <input type="text" id="userinput4" class="form-control " placeholder="Village Name" name="village" value="{{$case->village}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control" for="userinput3">DISTRICT</label>
                                                            <div class="col-md-9">
                                                                <input type="text" id="userinput3" class="form-control " placeholder="District" name="district" value="{{$case->district}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control" for="userinput4">HEALTH FACILITY<span class="text-danger">*</span></label>
                                                            <div class="col-md-9">
                                                                <input type="text" id="userinput4" class="form-control " placeholder="health facility" name="health_facility" required="" value="{{$case->health_facility}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control" for="userinput5">PHYS/SURG</label>
                                                            <div class="col-md-9">
                                                                <input class="form-control " type="text" placeholder="PHYS/SURG" id="userinput5" name="phys_surg" value="{{$case->phys_surg}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control" for="userinput6">DATE OF REQUEST<span class="text-danger">*</span></label>
                                                            <div class="col-md-9">
                                                                <input class="form-control " type="date" placeholder="" id="userinput6" name="date_of_request" required="" value="{{$case->date_of_request}}">
                                                            </div>
                                                        </div>

                                                        

                                                
                                                    </div>
                                                    <div class="col-md-6">
                                                      <div class="form-group row">
                                                            <label class="col-md-3 label-control">UPLOAD DOC</label>
                                                            <div class="col-md-9">
                                                                <input class="form help" type="file" placeholder="Contact Number" name="case_doc">
                                                            </div>
                                                        </div>
                                                      <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput6">NATURE OF SPECIMEN<span class="text-danger">*</span></label>
                                                    <div class="col-md-9">
                                                        <select id="projectinput6" name="specimen" class="form-control" required="">
                                                            <option selected="" value="" disabled="">Select</option>
                                                            <option value="Histology">Histology</option>
                                                            <option value="Cytology">Cytology</option>
                                                            
                                                        </select>
                                                    </div>
                                                </div>
                                                    </div>
                                                        </div>

                                                   <div class="row">
                                                    <div class="col-md-6">
                                                      <div class="form-group row">
                                                            <label class="col-md-3 label-control" for="userinput8">CLINICAL SUMMARY</label>
                                                            <div class="col-md-9">
                                                                <textarea id="userinput8" rows="6" class="form-control " name="clinical_summary" placeholder="Bio">{{$case->clinical_summary}}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                   </div>
                                                        
                                                    </div>
                                                    <div class="form-actions center">
                                                <button type="button" class="btn btn-danger mr-1">
                                                    <i class="ft-x"></i> Cancel
                                                </button>
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check-square-o"></i> Save
                                                </button>
                                            </div>
                                                </form>
                                       
                                     </div>
                                 </div>
                             </div>                         
                         </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
                                        
@endsection


