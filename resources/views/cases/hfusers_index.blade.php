@extends('layouts.master')

@section('content')
  <!-- Content Header (Page header) -->
                <section class="content-header">
                     <form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
                            <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </form>   
                    <div class="header-icon">
                        <i class="fa fa-vcard"></i>
                    </div>
                    <div class="header-title">
                        <h1> Cases</h1>
                        <small> Case List</small>
                        <ol class="breadcrumb hidden-xs">
                            <li><a href="index.html"><i class="pe-7s-home"></i> Home</a></li>
                            <li class="active">Cases</li>
                        </ol>
                    </div>
                </section>
            <!-- Main content -->
                <section class="content">
                  <div class="row">
                            <div class="col-sm-12">
                              @if(session()->get('success'))
                                  <div class="alert alert-success">
                                    {{ session()->get('success') }}  
                                  </div>
                                @endif
                                <div class="panel panel-bd" data-inner-id="a7HgTIWv4t" data-index="0">
                                    <div class="panel-heading">
                                        <div class="btn-group"> 
                                            <a class="btn btn-success" href="{{ route('create_case')}}"> <i class="fa fa-plus"></i>  Add Case</a>  
                                        </div>
                                       
                                      <!--    <a href="{{ route('get_pdf') }}" class="btn btn-secondary mb-2" style="float: right;">Export PDF</a> -->
                                      </div>
                                    <div class="panel-body">
                              <div class="table-responsive">
                                <table id="datatable" class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline">  
                                    <thead>
                                        <tr>
                                            <th>DATE CREATED</th>
                                            <th>REQUEST</th>
                                            <th>RESPONSE FROM MBC</th>
                                            @if(Auth::user()->role != "Health Facility User")
                                            <th>ACTIONS</th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($cases as $case)
                                        <tr>
                                             
                                             <td>{{$case->created_at->format('j, M Y')}}</td>
                                             <td>
                                                @if($case->submitted)
                                                  <span class="label-default label label-warning">Submitted</span>
                                                @else
                                                  <form method="POST" action="{{route('store_request')}}">
                                                      {{ csrf_field() }}
                                                      <input type="hidden" name="case_id" value="{{$case->id}}" />
                                                      <input type="submit" attr-title="{{$case->id}}"  class="btn btn-info btn-s" data-toggle="tooltip" data-placement="left" title="Click to submit request" value="Send Request">
                                                  </form>
                                                @endif
                                             </td>
                                             <td>
                                              @if($case->response_id != 0)
                                              
                                             
                                                        <p><a href="/responses/show/{{$case->response_id}}" target="_blank" class="btn btn-warning btn-s">View</a></p>
                                              @else
                                                 <span class="label-default label label-secondary">Not Received</span>


                                              @endif
                                            </td>
                                              @if(Auth::user()->role != "Health Facility User")

                                             <td class="btn-group">
                                                  <a href="/cases/show/{{$case->id}}" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="left" title="View"><i class="fa fa-hospital-o" aria-hidden="true"></i></a>
                                                <a href="/cases/edit/{{$case->id}}" class="btn btn-purple btn-xs" data-toggle="tooltip" data-placement="left" title="Edit" ><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                               <form action="{{ route('delete_case', $case->id)}}" method="post">
                                                  @csrf
                                                  @method('DELETE')
                                                 @if(Auth::user()->role == "admin" || Auth::user()->role == "Low Level Admin")

                                                  <button class="btn btn-danger btn-xs data-toggle="tooltip" data-placement="right" title="Delete" type="submit"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                                  @endif
                                                </form>
                                            </td>
                                            @endif
                                        </tr>
                                        @endforeach
                                   
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
</div>   </section> 

<script text="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable({
          
        //   dom: 'Bfrtip',
        //  buttons: [
        //     'copy', 'csv', 'excel', 'pdf', 'print'
        // ],
          columnDefs: [{ orderable: false, targets: [] }],

        });
    });
</script>
@endsection