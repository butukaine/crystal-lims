@extends('layouts.master')

@section('content')
  <!-- Content Header (Page header) -->
                <section class="content-header">
                     <form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
                            <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </form>   
                    <div class="header-icon">
                        <i class="fa fa-vcard"></i>
                    </div>
                    <div class="header-title">
                        <h1> Cases</h1>
                        <small> New Case</small>
                        <ol class="breadcrumb hidden-xs">
                            <li><a href="index.html"><i class="pe-7s-home"></i> Home</a></li>
                            <li class="active">Cases</li>
                        </ol>
                    </div>
                </section>
            <!-- Main content -->
                <section class="content">

                  <div class="row">
<!-- Form controls -->
                            <div class="col-sm-12 " data-lobipanel-child-inner-id="HmSnvsvMGa">
                                <div class="panel panel-bd " data-inner-id="HmSnvsvMGa" data-index="0">
                                    <div class="panel-heading ui-sortable-handle">
                                        <div class="btn-group"> 
                                            <a class="btn btn-primary" href="{{ route('index_cases')}}"> <i class="fa fa-list"></i>  Case List </a>  
                                        </div>
                                 </div>
                                    <div class="panel-body">

                                        <form class="col-sm-12" action="{{ route('store_doc')}}" method="POST" enctype="multipart/form-data">
                                        	@csrf
                                            @if ($errors->any())
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            
                                               <div class="form-group col-sm-6">
                                                    <label>Upload Document <small class="form_help"></small></label><span
                                                    class="required-field">*</span>
                                                    <input class="form-control" type="file" name="case_doc" required>
                                                </div>
                                           
                                              <div class="col-sm-12 text-center">
                                                 <a href="/cases" class="btn btn-warning">Back</a>
                                                 <button class="btn btn-success" type="submit">Upload</button>
                                             </div>
                                         </form>
                                     </div>
                                 </div>
                             </div>                         
                         </div>	
                </section> 
            <!-- /.content -->
@endsection
@section('scripts')

            <script type="text/javascript">
            $(function () {
                $('.select2-selection--single').select2();
                $('#datetimepicker1').datetime();
            });
        </script>
@endsection