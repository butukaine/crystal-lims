@extends('layouts.master')
    
@section('content')
<div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">{{session('title')}}</h3>
                </div>
                <div class="content-header-right col-md-8 col-12">
                    <div class="breadcrumbs-top float-md-right">
                        <div class="breadcrumb-wrapper mr-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a>
                                </li>
                                <li class="breadcrumb-item active">Cases
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>



<div class="content-body">
                <!-- Zero configuration table -->
                <section id="configuration">

<div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">List of Cases</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                      <a class="btn btn-success "  href="{{route('create_case')}}">
                                                        <i class="ft-plus"></i>
                                                        New Case
                                      </a>

                                  
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <div class="table-responsive">
                                            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                            
                                            
                                            <div class="row"><div class="col-sm-12">
                                            <table class="table table-striped table-bordered zero-configuration dataTable" id="datatable" role="grid" aria-describedby="DataTables_Table_0_info">
                                                <thead>
                                                    <tr role="row">
                                                    <th>PATIENT NAME</th
                                                    >
                                                    <th>SPECIMEN</th>
                                                     <th>DATE ADDED</th>
                                                      
                                                      <th>REQUEST</th>
                                                       <th>REPORT</th>
                                                       @if(Auth::user()->role != "Health Facility User")
                                            <th></th>
                                            @endif
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($cases as $case )
                                                    
                                                <tr>
                                                       <td>
                                              @if($case->patient_name != "Check in document")
                                              {{$case->patient_name}}
                                              @else
                                              <form action="{{route('download')}}" method="GET">
                                                            @csrf
                                                            <input type="hidden" name="path" value="/uploads/cases/{{$case->document}}">
                                                            <input type="submit" class="btn btn-secondary" value="{{$case->patient_name}}">
                                                        </form>
                                              @endif

                                            </td>
                                             <td>{{$case->specimen}}</td>
                                             <td>{{$case->created_at->isoFormat('dddd, Do MMMM YYYY')}}</td>
                                             <td>
                                                @if($case->submitted)
                                                  <span class="badge badge-pill badge-light">Submitted</span>
                                                @else
                                                  <form method="POST" action="{{route('store_request')}}">
                                                      {{ csrf_field() }}
                                                      <input type="hidden" name="case_id" value="{{$case->id}}" />
                                                      <input type="submit" attr-title="{{$case->id}}"  class="btn btn-info btn-s" data-toggle="tooltip" data-placement="left" title="Click to submit request" value="Send Request">
                                                  </form>
                                                @endif
                                                 </td>
                                             <td>
                                              @if($case->response_id != 0)
                                            
                                                        <p><a href="/reports/show/{{$case->response_id}}" target="_blank" class="btn btn-warning btn-s">View</a></p>
                                              @else
                                                 <span class="label-default label text-danger">Not Received</span>


                                              @endif
                                            </td>
                                              @if(Auth::user()->role != "Health Facility User")


                                             <td >
                                              <div class="btn-group mr-1 mb-1">
                                            <button class="btn btn-light dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Actions
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 40px, 0px);">
                                               <a href="/cases/show/{{$case->id}}" class="dropdown-item" title="View">View</a>
                                               <a href="/cases/edit/{{$case->id}}" class="dropdown-item" title="View">Edit</a>
                                               @if(Auth::user()->role == "admin" || Auth::user()->role == "Low Level Admin")
                                               <a href="#" onclick="deleteItem({{$case->id}})" class="dropdown-item">
                                                Delete</a>
                                              @endif
                                                </form>
                                            </div>
                                        </div>
                                             
                                                 
                                            </td>
                                            @endif
                                                    </tr>
                                                    @endforeach
                                                    
                                                    
                                                </tbody>
                                               
                                            </table></div></div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </section>
            </div>

 <script type="text/javascript">
      function deleteItem(id){
                Swal.fire({
                title: 'Are you sure?',
                text: 'You will not be able to recover this case!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#6610f2',
                cancelButtonColor: '#607D8B',
                confirmButtonText: 'Yes, delete it!',
                closeOnConfirm: false
            }).then((result) => {
              if(result.value){
                $.ajax({
                      url: '/cases/delete/'+id,
                      type: "DELETE",
                      data:{ _token: "{{csrf_token()}}" }
                  }).done(function() {

                      swal({
                          title: "Deleted", 
                          text: "Case has been successfully deleted", 
                          type: "success",
                          confirmButtonColor: '#6610f2',

                      }).then(function() {
                          location.href = '/cases';
                      });
                  });

              }

               
            });

              }
              
            </script>




                                        
@endsection


