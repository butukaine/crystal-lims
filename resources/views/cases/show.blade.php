@extends('layouts.master')
    
@section('content')
<div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">{{session('title')}}</h3>
                </div>
                <div class="content-header-right col-md-8 col-12">
                    <div class="breadcrumbs-top float-md-right">
                        <div class="breadcrumb-wrapper mr-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('index_cases')}}">Cases</a>
                                </li>
                                <li class="breadcrumb-item active">{{session('title')}}
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>



<div class="content-body">
    <!-- Zero configuration table -->
    <section id="configuration">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title text-primary">Case Details</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <a type="button" class="btn btn-outline-secondary" href="{{ route('download_case',  $case->id)}}">
                                <i class="ft-arrow-up-right"></i>
                                Export
                            </a>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                           
                  <div class="row">
<!-- Form controls -->
                            <div class="col-sm-12 " data-lobipanel-child-inner-id="HmSnvsvMGa">
                                <div class="panel panel-bd " data-inner-id="HmSnvsvMGa" data-index="0">
                                    
                                    <div class="panel-body">
                                            @if ($errors->any())
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                    <div class="col-sm-4">
                                                        <label><b>NAME OF PATIENT:</b></label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <p> {{$case->patient_name}}</p>          
                                                    </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <div class="col-sm-4">
                                                    <label><b>PATIENT'S I.D NUMBER:</b></label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                    <p>{{$case->patient_number}}</p>
                                                    </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-4">
                                                    <label><b>GENDER:</b></label><br>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <p>{{$case->gender}}</p>
                                                        
                                                    </div>
                                            </div>
                                             <div class="form-group row">
                                                <div class="col-sm-4">
                                                        <label><b>AGE:</b></label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                    @if(!$case->age == null)
                                                      <p>{{$case->age}}</p>
                                                    @endif

                                                    

                                                    </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-4">
                                                    <label><b>VILLAGE/ZONE:</b></label>
  
                                                    </div>
                                                    <div class="col-sm-8">
                                                    <p>{{$case->village}}</p>

                                                    </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-4">
                                                    <label><b>PARISH:</b></label>
 
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <p>{{$case->parish}}</p>
                                                    </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <div class="col-sm-4">
                                                 <label><b>SUB-COUNTY:</b></label>                                                     
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <p>{{$case->sub_county}}</p>
                                                    </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-4">
                                                <label><b>DISTRICT:</b></label>                                                        
                                                    </div>
                                                    <div class="col-sm-8">
                                                <p>{{$case->district}}</p>
                                                    </div>
                                            </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                <div class="col-sm-4">
                                                <label><b>HEALTH UNIT:</b></label>                                                        
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <p>{{$case->health_facility}}</p>
                                                    </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-4">
                                                <label><b>PHYS/SURG:</b></label>                                                        
                                                    </div>
                                                    <div class="col-sm-8">
                                                <p>{{$case->phys_surg}}</p>
                                                    </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-4">
                                                <label><b>NATURE OF SPECIMEN:</b></label>                                                        
                                                    </div>
                                                    <div class="col-sm-8">
                                                <p>{{$case->specimen}}</p>
                                                    </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-4">
                                                <label><b>DATE OF REQUEST:</b></label>                                                        
                                                    </div>
                                                    <div class="col-sm-8">
                                                <p>{{$case->date_of_request}}</p>
                                                    </div>
                                            </div>
                                             <div class="form-group row">
                                                <div class="col-sm-4">
                                                <label><b>DATE CREATED:</b></label>                                                        
                                                    </div>
                                                    <div class="col-sm-8">
                                                <p>{{$case->created_at->format('j, M Y')}}</p>
                                                    </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-4">
                                                <label><b>CLINICAL SUMMARY:</b></label>                                                        
                                                    </div>
                                                    <div class="col-sm-8">
                                                <p>{{$case->clinical_summary}}</p>
                                                    </div>
                                            </div>
                                             <div class="form-group row ">
                                                    <div class="col-sm-4">
                                                        <label><b>UPLOADED DOCUMENT:</b> </label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                          <td>

                                              <!--       <a href="{{asset('uploads/cases')}}/{{$case->document}}" download="{{ $case->document }}"> <button type="button" class="btn btn-primary">Dowanload</button></a> -->

                                                        @if($case->document != "")
                                                        <form action="{{route('download')}}" method="GET">
                                                            @csrf
                                                            <input type="hidden" name="path" value='{{$case->document}}'>
                                                            <input type="submit" class="btn btn-outline-primary btn-xs" value="Download">
                                                        </form>
                                                        @else
                                                            <p>No file available</p>
                                                        @endif                                                        
                                                    </div>
                                                </div> 
                                                </div>
                                            </div>
                                              
                                            
                                       
                                     </div>
                                 </div>
                             </div>                         
                         </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
                                        
@endsection


