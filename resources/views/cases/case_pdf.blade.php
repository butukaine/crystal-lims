
  <div class="panel-body">
      <img class="text-center" alt="Logo" src="{{public_path('/logo-red.png')}}"
                     width=230" height="100"> 
                                           <h1>{{$case->code}}</h1>
                                           <hr>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                    <div class="col-xs-4">
                                                        <strong>NAME OF PATIENT:</strong>
                                                    </div>
                                                    <div class="col-xs-8">
                                                        <p> {{$case->patient_name}}</p>          
                                                    </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <div class="col-xs-4">
                                                    <strong>PATIENT'S I.D NUMBER:</strong>
                                                    </div>
                                                    <div class="col-xs-8">
                                                    <p>{{$case->patient_number}}</p>
                                                    </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-xs-4">
                                                    <strong>GENDER:</strong><br>
                                                    </div>
                                                    <div class="col-xs-8">
                                                        <p>{{$case->gender}}</p>
                                                        
                                                    </div>
                                            </div>
                                             <div class="form-group row">
                                                <div class="col-xs-4">
                                                        <strong>AGE:</strong>
                                                    </div>
                                                    <div class="col-xs-8">
                                                      <p>{{$case->age}}</p>

                                                    </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-xs-4">
                                                    <strong>VILLAGE/ZONE:</strong>
  
                                                    </div>
                                                    <div class="col-xs-8">
                                                    <p>{{$case->village}}</p>

                                                    </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-xs-4">
                                                    <strong>PARISH:</strong>
 
                                                    </div>
                                                    <div class="col-xs-8">
                                                        <p>{{$case->parish}}</p>
                                                    </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <div class="col-xs-4">
                                                 <strong>SUB-COUNTY:</strong>                                                     
                                                    </div>
                                                    <div class="col-xs-8">
                                                        <p>{{$case->sub_county}}</p>
                                                    </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-xs-4">
                                                <strong>DISTRICT:</strong>                                                        
                                                    </div>
                                                    <div class="col-xs-8">
                                                <p>{{$case->district}}</p>
                                                    </div>
                                            </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                <div class="col-xs-4">
                                                <strong>HEALTH UNIT:</strong>                                                        
                                                    </div>
                                                    <div class="col-xs-8">
                                                        @if($case->health_facility != 0)
                                                        <p>{{$health_unit->name}}</p>
                                                        @else
                                                        <p>{{$case->other_facility}}</p>
                                                        @endif                                                    
                                                    </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-xs-4">
                                                <strong>PHYS/SURG:</strong>                                                        
                                                    </div>
                                                    <div class="col-xs-8">
                                                <p>{{$case->phys_surg}}</p>
                                                    </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-xs-4">
                                                <strong>NATURE OF SPECIMEN:</strong>                                                        
                                                    </div>
                                                    <div class="col-xs-8">
                                                <p>{{$case->specimen}}</p>
                                                    </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-xs-4">
                                                <strong>DATE OF REQUEST:</strong>                                                        
                                                    </div>
                                                    <div class="col-xs-8">
                                                <p>{{$case->date_of_request}}</p>
                                                    </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-xs-4">
                                                <strong>DATE CREATED:</strong>                                                        
                                                    </div>
                                                    <div class="col-xs-8">
                                                <p>{{$case->created_at->format('j, M Y')}}</p>
                                                    </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-xs-4">
                                                <strong>CLINICAL SUMMARY:</strong>                                                        
                                                    </div>
                                                    <div class="col-xs-8">
                                                <p>{{$case->clinical_summary}}</p>
                                                    </div>
                                            </div>
                                             <div class="form-group row ">
                                                    <div class="col-xs-4">
                                                        <strong>UPLOADED DOCUMENT: </strong>
                                                    </div>
                                                    <div class="col-xs-8">

                                                        @if($case->document != "")
                                                        <p>{{$case->document}}</p>
                                                      
                                                        @else
                                                            <p>No file available</p>
                                                        @endif                                                        
                                                    </div>
                                                </div> 
                                                </div>
                                            </div>
                                     </div>