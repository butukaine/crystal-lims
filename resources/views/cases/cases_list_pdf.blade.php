<head>
  <style type="text/css">
    table, th, td {
    border: 0.5px solid green;
      border-collapse: collapse;
}
th, td {
  padding: 15px;
}
table {
  border-spacing: 5px;
}
  </style>
</head>
  <body>
    <div>
            <img class="text-center" alt="Logo" src="{{public_path('/logo-red.png')}}"
                     width=230" height="100"> 
          <h1>List of Cases</h1>
          <p>Download Date: {{$date}}</p>
          <hr>
    </div>
  <div class="table-responsive" style="padding-top: 15px;">
                                <table id="datatable" class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline">  
                                    <thead>
                                        <tr>
                                            <th>PATIENT NAME</th>
                                            <th>AGE</th>
                                            <th>GENDER</th>
                                            <th>NATURE OF SPECIMEN</th>
                                            <th>DATE OF REQUEST</th>
                                            <th>DATE CREATED</th>
                                            <th>REQUEST</th>
                                            <th>RESPONSE FROM MBC</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($cases as $case)
                                        <tr>
                                             
                                             <td>{{$case->patient_name}}</td>
                                             <td>{{$case->age}}</td>
                                             <td>{{$case->gender}}</td>
                                             <td>{{$case->specimen}}</td>
                                             <td>{{$case->date_of_request}}</td>
                                             <td>{{$case->created_at->format('j, M Y')}}</td>

                                             <td>
                                                @if($case->submitted)
                                                  <p class="label-default label label-warning">Submitted</p>
                                                @else
                                                  <p class="label-default label label-warning" style="color: red;">Pending Submission</p>

                                            
                                                @endif
                                             </td>
                                             <td>
                                              @if($case->response_id != 0)
                                            
                                                 <p class="label-default label label-secondary" style="color: green"> Received</p>

                                              @else
                                                 <p class="label-default label label-secondary">Not Received</p>


                                              @endif
                                            </td>
                                             
                                        </tr>
                                        @endforeach
                                   
                </tbody>
            </table>
        </div>
  </body>
