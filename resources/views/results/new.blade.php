@extends('layouts.master')


@section('content')
  <!-- Content Header (Page header) -->
                <section class="content-header">
                    

                    <div class="header-icon">
                        <i class="fa fa-search"></i>
                    </div>
                    <div class="header-title">
                        <h1> Searches</h1>
                        <small> Add result</small>
                        <ol class="breadcrumb hidden-xs">
                            <li><a href="/requests"><i class="pe-7s-home"></i> Home</a></li>
                            <li class="active">Search</li>
                        </ol>
                    </div>
                </section>
            <!-- Main content -->
                <section class="content">
                  <div class="row">
                            <div class="col-sm-12">

                                <div class="panel panel-bd" data-inner-id="a7HgTIWv4t" data-index="0">
                                   <div class="panel-heading">
                                           <div> 
                                            <a class="btn btn-success"> <i class="fa fa-upload"></i>  Upload Results</a>  
                                            
                                           </div>
                                      </div>
                                    <div class="panel-body">
                                        @if(session()->get('success'))
                                  <div class="alert alert-success">
                                    {{ session()->get('success') }}  
                                  </div>
                                @endif
                                @if(session()->get('danger'))
                                  <div class="alert alert-danger">
                                    {{ session()->get('danger') }}  
                                  </div>
                                @endif
                                    	<div class="col-sm-4"></div>
                                   		<div class="col-sm-4 text-center">
                                             <form class="col-sm-12" action="{{ route('store_search_result')}}" method="POST" enctype="multipart/form-data" autocomplete="off">
                                              @csrf
                                                <input type="hidden" name="search_id" value="{{$search->id}}" />
                                                <div class="col-sm-12 form-group">
                                                    <label>BIOPSY NUMBER</label>
                                                    <input class="form-control" type="text" name="biopsy_no" /> 
                                                </div>
                                                <div class=" form-group col-sm-12" style="margin-top: 20px;">
                                                    <label class="form-label">Upload Document</label>
                                                    <input class="form-control" type="file" name="file">
                                                </div>
                                                <div class="col-sm-12 form-group" style="margin-top: 20px;">
                                                    <label>Comment</label> (Optional)
                                                    <textarea  class="form-control" type="text" name="comments" rows="2"></textarea>
                                                </div>
                                                <div class="col-sm-12" style="margin-top: 20px;">
                                                    <button type="submit" class="btn btn-success">SAVE</button>
                                                    <a href="/searches" type="button" class="btn btn-default">DONE</a>

                                                </div>
                                            </form>
                                        </div>
                                         <div class="col-sm-4"></div>

                             
    </div>
</div>
</div>

</div> 

</section> 
<!-- /.content -->
@endsection