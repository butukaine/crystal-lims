@extends('layouts.master')

@section('content')
  <!-- Content Header (Page header) -->
                <section class="content-header">
                     <form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
                            <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </form>   
                    <div class="header-icon">
                        <i class="fa fa-vcard"></i>
                    </div>
                    <div class="header-title">
                        <h1> Search Results</h1>
                        <small> List</small>
                        <ol class="breadcrumb hidden-xs">
                            <li><a href="index.html"><i class="pe-7s-home"></i> Home</a></li>
                            <li class="active">Search Results</li>
                        </ol>
                    </div>
                </section>
            <!-- Main content -->
                <section class="content">
                  <div class="row">
                            <div class="col-sm-12">
                              @if(session()->get('success'))
                                  <div class="alert alert-success">
                                    {{ session()->get('success') }}  
                                  </div>
                                @endif
                                <div class="panel panel-bd" data-inner-id="a7HgTIWv4t" data-index="0">
                                    <div class="panel-heading">
                                        <div class="btn-group"> 
                                            <a class="btn btn-success" href="#"> <i class="fa fa-list"></i> List </a>  
                                        </div>
                                       
                                      <!--    <a href="{{ route('get_pdf') }}" class="btn btn-secondary mb-2" style="float: right;">Export PDF</a> -->
                                      </div>
                                    <div class="panel-body">
                              <div class="table-responsive">
                                <table id="datatable" class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline">  
                                    <thead>
                                        <tr>
                                            <th>ACADEMICIAN</th>
                                            <th>DATE SEARCHED</th>
                                            <th>BIOPSY NO</th>
                                            <th>PATHOLOGY REPORT</th>
                                            <th>ADD/CHANGE</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($results as $res)
                                        <tr>
                                             
                                             <td>{{$res->academician}}</td>
                                             <td>{{$res->date_searched}}</td>
                                             <td>{{$res->biopsy_no}}</td>
                                             <td>{{$res->pathology_report}}</td>
                                             <td>
                                               <form action="{{ route('upload_report', $res->id)}}" method="post">
                                                  @csrf
                                                  @method('POST')
                                                     
                                                    {{ csrf_field() }}
                                                <input type="hidden" name="result_id" value="{{$res->id}}" />
                                                <input type="file" name="pathology_report">
                                                <button class="btn btn-primary btn-xs data-toggle="tooltip" data-placement="right" title="Add/Change pathology report" type="submit"> UPDATE</button>                                                 
                                                </form>

                                             </td>
                                             <td>
                                                <form action="{{ route('enable_viewing', $res->id)}}" method="post">
                                                  @csrf
                                                  @method('POST')
                                                     
                                                       <button class="btn btn-primary btn-s data-toggle="tooltip" data-placement="right" title="Click to allow academician to see results" type="submit"><i class="fa fa-search" aria-hidden="true"></i> SEND TO VIEW</button>                                                 
                                                </form>
                                             
                                            </td>
                                        </tr>
                                        @endforeach
                                   
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
</div>   </section> 

<script text="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable({
          
        //   dom: 'Bfrtip',
        //  buttons: [
        //     'copy', 'csv', 'excel', 'pdf', 'print'
        // ],
          columnDefs: [{ orderable: false, targets: [4,5] }],

        });
    });
</script>
@endsection