@extends('layouts.master')

@section ('content')
<div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">Dashboard</h3>
                </div>
                
            </div>
            <div class="content-body">
                <!-- Bar charts section start -->
                <section id="chartjs-bar-charts">
                     <div class="row">
                        <div class="col-xl-3 col-lg-6 col-12">
                            <a href="{{route('index_cases')}}">
                            <div class="card bg-gradient-x-purple-blue">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media d-flex">
                                            <div class="align-self-top">
                                                <i class="icon-layers icon-opacity text-white font-large-4 float-left"></i>
                                            </div>
                                            <div class="media-body text-white text-right align-self-bottom mt-3">
                                                <span class="d-block mb-1 font-medium-1">Total Cases</span>
                                                <h1 class="text-white mb-0">{{$cases}}</h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-12">
                            <a href="{{route('index_requests')}}">
                            <div class="card bg-gradient-x-purple-red">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media d-flex">
                                            <div class="align-self-top">
                                                <i class="icon-arrow-down icon-opacity text-white font-large-4 float-left"></i>
                                            </div>
                                            <div class="media-body text-white text-right align-self-bottom mt-3">
                                                <span class="d-block mb-1 font-medium-1">Total Requests</span>
                                                <h1 class="text-white mb-0">{{$requests}}</h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-12">
                            <a href="{{route('index_responses')}}">
                            <div class="card bg-gradient-x-blue-green">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media d-flex">
                                            <div class="align-self-top">
                                                <i class="icon-doc icon-opacity text-white font-large-4 float-left"></i>
                                            </div>
                                            <div class="media-body text-white text-right align-self-bottom mt-3">
                                                <span class="d-block mb-1 font-medium-1">Total Reports</span>
                                                <h1 class="text-white mb-0">{{$responses}}</h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-12">
                            <a href="{{route('index_clinicians')}}">
                            <div class="card bg-gradient-x-orange-yellow">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media d-flex">
                                            <div class="align-self-top">
                                                <i class="icon-users icon-opacity text-white font-large-4 float-left"></i>
                                            </div>
                                            <div class="media-body text-white text-right align-self-bottom mt-3">
                                                <span class="d-block mb-1 font-medium-1">Total Users</span>
                                                <h1 class="text-white mb-0">{{$users}}</h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                    </div>
                    <!-- Column Chart -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Cases Vs Reports</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <div class="height-600">
                                            <!-- <canvas id="column-chart"></canvas> -->
                                            <canvas id="barChart" height="100"></canvas>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <!--  <div class="col-md-4 col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Requests Chart</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                   
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <div class="height-400">
                                            <canvas id="simple-doughnut-chart"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </section>
              
            </div>

<!-- ChartJs JavaScript -->
       
<script>

         $(function(){
            var APP_URL = {!! json_encode(url('/')) !!}

            $.ajax({
                url: APP_URL + '/get_bar_graph_stats',
                success: function(data){
                    console.log("onSuccess: Cases-> " + data.cases + " Response -> "+ data.responses);
                    drawBarGraph(data);
                }

            });

         });       

    
    function drawBarGraph(data) {
            var ctx = document.getElementById("barChart");
            var myChart = new Chart(ctx, {
                type: "bar",
                options: {
                    elements: {
                        rectangle: {
                            borderWidth: 2,
                            borderColor: "rgb(0, 255, 0)",
                            borderSkipped: "bottom"
                        }
                    },
                    responsive: !0,
                    maintainAspectRatio: !1,
                    responsiveAnimationDuration: 500,
                    legend: {
                        position: "top"
                    },
                    scales: {
                        xAxes: [{
                            display: !0,
                            gridLines: {
                                color: "#f3f3f3",
                                drawTicks: !1
                            },
                            scaleLabel: {
                                display: !0
                            }
                        }],
                        yAxes: [{
                            display: !0,
                            gridLines: {
                                color: "#f3f3f3",
                                drawTicks: !1
                            },
                            scaleLabel: {
                                display: !0
                            }
                        }]
                    },
                    title: {
                        display: !0,
                        text: ""
                    }
                },
                data: {
                    labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                    datasets: [{
                        label: "Cases",
                        data: data.cases,
                        backgroundColor: "#28D094",
                        hoverBackgroundColor: "rgba(40,208,148,.9)",
                        borderColor: "transparent"
                    }, {
                        label: "Reports",
                        data: data.responses,
                        backgroundColor: "#FF4961",
                        hoverBackgroundColor: "rgba(255,73,97,.9)",
                        borderColor: "transparent"
                    }]
                }
            })
    }
</script>

@endsection