
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Mainstream Biomedical Centre - Admin Dashboard</title>


    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="/favicon.png" type="image/x-icon">
   <!-- Start Global Mandatory Style
   =====================================================================-->
   <!-- jquery-ui css -->
   <link href="/assets/plugins/jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet" type="text/css"/>

        <!-- jQuery -->
        <script src="/assets/plugins/jQuery/jquery-1.12.4.min.js" type="text/javascript"></script>
        <!-- jquery-ui --> 
        <script src="/assets/plugins/jquery-ui-1.12.1/jquery-ui.min.js" type="text/javascript"></script>
   <link rel="stylesheet" type="text/css" href="/css/datatables.min.css">
   <!-- Bootstrap -->
   <link href="/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
   <!-- Bootstrap rtl -->
   <!--<link href="assets/bootstrap-rtl/bootstrap-rtl.min.css" rel="stylesheet" type="text/css"/>-->
   <!-- Lobipanel css -->
   <link href="/assets/plugins/lobipanel/lobipanel.min.css" rel="stylesheet" type="text/css"/>
   <!-- Pace css -->
   <link href="/assets/plugins/pace/flash.css" rel="stylesheet" type="text/css"/>
   <!-- Font Awesome -->
   <link href="/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
   <!-- Pe-icon -->
   <link href="/assets/pe-icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet" type="text/css"/>
   <!-- Themify icons -->
   <link href="/assets/themify-icons/themify-icons.css" rel="stylesheet" type="text/css"/>
        <!-- End Global Mandatory Style
        =====================================================================-->
        <!-- Start page Label Plugins 
        =====================================================================-->
            <link rel="stylesheet" href="/css/select2.min.css">

        <!-- Toastr css -->
        <link href="/assets/plugins/toastr/toastr.css" rel="stylesheet" type="text/css"/>
        <!-- Emojionearea -->
        <link href="/assets/plugins/emojionearea/emojionearea.min.css" rel="stylesheet" type="text/css"/>
        <!-- Monthly css -->
        <link href="/assets/plugins/monthly/monthly.css" rel="stylesheet" type="text/css"/>
        <!-- End page Label Plugins 
        =====================================================================-->
        <!-- Start Theme Layout Style
        =====================================================================-->
        <!-- Theme style -->
        <link href="/assets/dist/css/stylehealth.min.css" rel="stylesheet" type="text/css"/>
        <!--<link href="assets/dist/css/stylehealth-rtl.css" rel="stylesheet" type="text/css"/>-->
        <!-- End Theme Layout Style
        =====================================================================-->
        <style type="text/css">
          .panel-body{
/*            background:white;
*/          }
        .search-body{
            max-width: 1200px;
            z-index: 100;
            margin: 0 auto;
            padding: 15px;
            padding-top: 70px;
        }
        .nav-btn{
            margin-top:10px;
        }
        </style>
    </head>
    <body class="hold-transition sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">
            <header class="main-header">
               
                 <a href="/home" class="logo" style="background-color: transparent;">  <!-- Logo -->
                  <img alt="Logo" src="/logo.jpg"
                     width=230" height="70">                
                </a>
                <!-- Header Navbar -->
                <nav class="navbar navbar-static-top ">
                 
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="nav-item">
                               <a class="btn btn-primary btn-s btn-outline nav-btn" href="{{route('start_search')}}">
                                New Search
                               </a>

                            </li>                            <li class="nav-item">
                               <a class="btn btn-warning nav-btn" href="{{route('mysearches')}}">
                                My Searches
                               </a>
                            </li>
                            <li class="dropdown dropdown-user admin-user">
                            
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
                                <div class="user-image">
                                <img src="/assets/dist/img/avatar4.png" class="img-circle" height="40" width="40" alt="User Image">
                                </div>
                                </a>
                                <ul class="dropdown-menu">   
                                    <li class="nav-item">
                                            <a class="" data-toggle="dropdown">
                                                {{ Auth::user()->name }}
                                            </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="fa fa-user-circle" href="{{route('profile')}}" aria-haspopup="true" aria-expanded="false" v-pre>
                                            Profile
                                        </a>

                                    </li>                                  
                                    <li class="nav-item">
                                        <a class="fa fa-sign-out nav-link" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
  
            <!-- =============================================== -->
            <!-- Content Wrapper. Contains page content -->
            <div class="search-body">
              @yield('content')

            </div> <!-- /.content-wrapper -->
         <!--    <footer class="main-footer">
                <div class="pull-right hidden-xs"> <b>Version</b> 1.0</div>
                <strong>Copyright &copy; 2019 <a href="#">Mainstream Biomedical Center</a>.</strong> All rights reserved.
            </footer> -->
        </div> <!-- ./wrapper -->
        <!-- ./wrapper -->
         <!-- Start Core Plugins
        =====================================================================-->
        <!-- Bootstrap -->
        <script src="/assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- lobipanel -->
        <script src="/assets/plugins/lobipanel/lobipanel.min.js" type="text/javascript"></script>
        <!-- Pace js -->
        <script src="/assets/plugins/pace/pace.min.js" type="text/javascript"></script>
        <!-- SlimScroll -->
        <script src="/assets/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <!-- FastClick -->
        <script src="/assets/plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
        <!-- Hadmin frame -->
        <script src="/assets/dist/js/custom1.js" type="text/javascript"></script>
        <script type="text/javascript" src="/js/datatables.min.js"></script>
        <!-- End Core Plugins
        =====================================================================-->
        <!-- Start Page Lavel Plugins
        =====================================================================-->
        <!-- Toastr js -->
        <script src="/assets/plugins/toastr/toastr.min.js" type="text/javascript"></script>
        <!-- Sparkline js -->
        <script src="/assets/plugins/sparkline/sparkline.min.js" type="text/javascript"></script>
        <!-- Data maps js -->
        <script src="/assets/plugins/datamaps/d3.min.js" type="text/javascript"></script>
        <script src="/assets/plugins/datamaps/topojson.min.js" type="text/javascript"></script>
        <script src="/assets/plugins/datamaps/datamaps.all.min.js" type="text/javascript"></script>
        <!-- Counter js -->
        <script src="/assets/plugins/counterup/waypoints.js" type="text/javascript"></script>
        <script src="/assets/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
        <!-- ChartJs JavaScript -->
        <script src="/assets/plugins/chartJs/Chart.min.js" type="text/javascript"></script>
        <script src="/assets/plugins/emojionearea/emojionearea.min.js" type="text/javascript"></script>
        <!-- Monthly js -->
        <script src="/assets/plugins/monthly/monthly.js" type="text/javascript"></script>
        <!-- Data maps -->
        <script src="/assets/plugins/datamaps/d3.min.js" type="text/javascript"></script>
        <script src="/assets/plugins/datamaps/topojson.min.js" type="text/javascript"></script>
        <script src="/assets/plugins/datamaps/datamaps.all.min.js" type="text/javascript"></script>
        <script src="/js/select2.full.min.js"></script>

        <!-- End Page Lavel Plugins
        =====================================================================-->
        <!-- Start Theme label Script
        =====================================================================-->
        <!-- Dashboard js -->
        <script src="/assets/dist/js/custom.js" type="text/javascript"></script>

        <!-- End Theme label Script
        =====================================================================-->

    </body>
</html>