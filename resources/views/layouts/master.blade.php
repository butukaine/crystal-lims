
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Chameleon Admin is a modern Bootstrap 4 webapp &amp; admin dashboard html template with a large number of components, elegant design, clean and organized code.">
    <meta name="keywords" content="admin template, Chameleon admin template, dashboard template, gradient admin template, responsive admin template, webapp, eCommerce dashboard, analytic dashboard">
    <meta name="author" content="ThemeSelect">
    <title>{{session('title')}} | {{config('app.name')}}</title>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/vendors.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/components.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/core/colors/palette-gradient.css">
        <link rel="stylesheet" type="text/css" href="/app-assets/fonts/simple-line-icons/style.min.css">
            <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/extensions/sweetalert.css">


    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
    <!-- END: Custom CSS-->


   <!-- jquery-ui css -->
   <link href="/app-assets/css/plugins/ui/jqueryui.min.css" rel="stylesheet" type="text/css"/>

        <!-- jQuery -->
        <script src="/app-assets/js/core/libraries/jquery.min.js" type="text/javascript"></script>
        <!-- jquery-ui --> 
        <script src="/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-gradient-x-purple-blue" data-col="2-columns">

    <!-- BEGIN: Header-->
     <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light">
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="collapse navbar-collapse show" id="navbar-mobile">
                    <ul class="nav navbar-nav mr-auto float-left">
                        <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                        <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
                        <li class="nav-item d-none d-md-block"><a class="nav-link nav-link-expand" href="#"><i class="ficon ft-maximize"></i></a></li>
                   
                    
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- END: Header-->


    <!-- BEGIN: Main Menu-->
    <div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true" data-img="/app-assets/images/backgrounds/02.jpg">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto text-center"><a  class="navbar-brand text-center"href="index.html"><img style="height:70px" alt="logo" src="/logo-red.png" />
                        
                    </a></li>
                <li class="nav-item d-md-none"><a class="nav-link close-navbar"><i class="ft-x"></i></a></li>
            </ul>
        </div>
        <div class="navigation-background"></div>
       <div class="main-menu-content mt-2">
            <ul class="navigation navigation-main mt-2" id="main-menu-navigation" data-menu="menu-navigation">
                 @if(Auth::user()->role == "admin")
                <li class=" nav-item"><a href="{{route('home')}}"><i class="ft-home"></i><span class="menu-title" data-i18n="">Dashboard</span></a>
                  
                </li>
                <li class="nav-item">
                    <a href="{{route('index_cases')}}"><i class="ft-layers"></i><span class="menu-title" data-i18n="">Cases</span></a>
                   
                </li>
                <li class=" nav-item"><a href="{{route('index_requests')}}"><i class="ft-arrow-down"></i><span class="menu-title" data-i18n="">Requests</span></a>
                   
                </li>
                <li class=" nav-item"><a href="{{route('index_responses')}}"><i class="ft-layout"></i><span class="menu-title" data-i18n="">Reports</span></a>
                    
                </li>
            
                <li class=" nav-item"><a href="#"><i class="ft-users"></i><span class="menu-title" data-i18n="">Users</span></a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="/clinicians">Clinicians</a>
                          
                        </li>                    
                        <li><a class="menu-item" href="/admin">Low level Admins</a> 
                        </li>
                        
                        
                    </ul>
                </li>
                @endif

                 @if(Auth::user()->role == "Clinician")
                  <li class="nav-item">
                    <a href="{{route('index_cases')}}"><i class="ft-layers"></i><span class="menu-title" data-i18n="">Cases</span></a>
                    </a>
                  </li>

                  @endif

                    @if(Auth::user()->role == "Low Level Admin")
                 <li class=" nav-item"><a href="{{route('index_requests')}}"><i class="ft-arrow-down"></i><span class="menu-title" data-i18n="">Requests</span></a>
                   
                </li>
                <li class=" nav-item"><a href="{{route('index_responses')}}"><i class="ft-layout"></i><span class="menu-title" data-i18n="">Reports</span></a>
                    
                </li>

                  @endif
               
                <li class=" nav-item">
                     <a   href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                         <i class="ft-power"></i>
                            Logout

                        </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
            
                </li>
               
               
            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-wrapper-before"></div>
           @yield('content')
        </div>
    </div>
    <!-- END: Content-->


    <!-- BEGIN: Vendor JS-->
    <script src="/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="/app-assets/vendors/js/charts/chart.min.js" type="text/javascript"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="/app-assets/js/core/app-menu.js" type="text/javascript"></script>
    <script src="/app-assets/js/core/app.js" type="text/javascript"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="/app-assets/js/scripts/charts/chartjs/bar/column.js" type="text/javascript"></script>
    <script src="/app-assets/js/scripts/charts/chartjs/bar/bar.js" type="text/javascript"></script>
    <script src="/app-assets/js/scripts/charts/chartjs/bar/column-stacked.js" type="text/javascript"></script>
    <script src="/app-assets/js/scripts/charts/chartjs/line/line.js" type="text/javascript"></script>
    <script src="/app-assets/js/scripts/charts/chartjs/line/line-area.js" type="text/javascript"></script>
    <script src="/app-assets/js/scripts/charts/chartjs/pie-doughnut/pie-simple.js" type="text/javascript"></script>
    <script src="/app-assets/js/scripts/charts/chartjs/pie-doughnut/doughnut-simple.js" type="text/javascript"></script>
    <script src="/app-assets/js/scripts/charts/chartjs/polar-radar/polar.js" type="text/javascript"></script>
    <script src="/app-assets/js/scripts/charts/chartjs/polar-radar/radar.js" type="text/javascript"></script>
    
    <script src="/app-assets/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>
    <script src="/app-assets/vendors/js/extensions/sweetalert2.all.js" type="text/javascript"></script>


    <script src="/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="/app-assets/js/scripts/tables/datatables/datatable-basic.js" type="text/javascript"></script>
    <!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>