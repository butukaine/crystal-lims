@extends('layouts.master')


@section('content')
  <!-- Content Header (Page header) -->
                <section class="content-header">
                     <form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
                            <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </form>   
                    <div class="header-icon">
                        <i class="fa fa-mail-forward"></i>
                    </div>
                    <div class="header-title">
                        <h1> Requests</h1>
                        <small> Request List</small>
                        <ol class="breadcrumb hidden-xs">
                            <li><a href="/requests"><i class="pe-7s-home"></i> Home</a></li>
                            <li class="active">Requests</li>
                        </ol>
                    </div>
                </section>
            <!-- Main content -->
                <section class="content">
                  <div class="row">
                            <div class="col-sm-12">
                              @if(session()->get('success'))
                                  <div class="alert alert-success">
                                    {{ session()->get('success') }}  
                                  </div>
                                @endif
                                <div class="panel panel-bd" data-inner-id="a7HgTIWv4t" data-index="0">
                                    <div class="panel-heading">
                                        <div> 
                                            <span class="btn btn-success"> <i class="fa fa-list"></i>  All Requests</span>  
                                               <a class="btn btn-default " style="float: right" href="{{ route('download_requests')}}"> Export to PDF </a> 
                                        </div>
                                    </div>
                                    <div class="panel-body">
                              <div class="table-responsive">
                              <table id="datatable" class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline">  
                                    <thead>
                                        <tr>
                                            <th>SUBMITTED BY</th>
                                            <th>CASE CODE</th>
                                            <th>PATIENT NAME</th>
                                            <th>DATE SUBMITTED</th>
                                            <th>CASE DOCUMENT</th>
                                            <th>PREPARE REPORT</th>
                                            @if(Auth::user()->role == "admin")
                                            <th>ACTIONS</th> @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($requests as $req)
                                        <tr>
                                             <td>{{$req->clinician->name}}</td>
                                             <td>{{$req->case->code}}</td>
                                             <td>{{$req->case->patient_name}}</td>
                                             <td>{{$req->date_submitted}}</td>
                                             <td> @if($req->case->document != "")
                                                        <form action="{{route('download')}}" method="GET">
                                                            @csrf
                                                            <input type="hidden" name="path" value='{{$req->case->document}}'>
                                                            <input type="submit" class="btn btn-warning btn-s" value="Download">
                                                        </form>
                                                        @else
                                                            <p class="text-secondary">Not Available</p>
                                                        @endif  
                                              </td>

                                             <td class="">
                                                @if($req->responded > 0)
                                                  <!-- <span class="label-default label label-warning">Responded</span> -->
                                                  <p><a href="/responses/show/{{$req->response_id}}" target="_blank" class="btn btn-default btn-s">View Report</a></p>

                                                @else
                                                     @if(Auth::user()->role == "Health Facility User" || Auth::user()->role == "Clinician")
                                                      @else
                                                      <a href="/responses/new/request_id={{$req->id}}"  class="btn btn-info btn-s" data-toggle="tooltip" data-placement="left" title="Click to prepare report">  START</a>
                                                      @endif 
                                                @endif
                                             </td>
                                             @if(Auth::user()->role == "admin")

                                             <td class="btn-group">
                                               <form action="{{ route('delete_request', $req->id)}}" method="post">
                                                  @csrf
                                                  @method('DELETE')
                                                     
                                                       <button class="btn btn-danger btn-s data-toggle="tooltip" data-placement="right" title="Delete" type="submit"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</button>                                                 
                                                </form>
                                            </td>
                                            @endif
                                        </tr>
                                        @endforeach
                                   
					                </tbody>
					            </table>
        </div>
    </div>
</div>
</div>
</div> 
</section> 
<!-- /.content -->

<script text="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable({
            columnDefs: [{ orderable: false, targets: [6] }],

        });
    });
</script>
@endsection