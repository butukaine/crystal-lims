@extends('admin.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/plugins/parsley/parsley.css') }}">

@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="header-icon">
			<i class="pe-7s-note2"></i>
		</div>
		<div class="header-title">
			<form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
				<div class="input-group">
					<input type="text" name="q" class="form-control" placeholder="Search...">
					<span class="input-group-btn">
						<button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
					</span>
				</div>
			</form>  
			<h1>Academican</h1>
			<small>Add Academican</small>
			<ol class="breadcrumb hidden-xs">
				<li><a href="{{ route('admin_dashboard') }}"><i class="pe-7s-home"></i> Home</a></li>
				<li class="active">Academican</li>
			</ol>
		</div>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- Form controls -->
			<div class="col-sm-12">
				<div class="panel panel-bd">
					<div class="panel-heading">
						<div class="btn-group"> 
							<a class="btn btn-primary" href="{{ route('academicans.index') }}"> <i class="fa fa-list"></i>  Academican List </a>  
						</div>
					</div>
					<div class="panel-body">
						<form action="{{ route('academicans.store') }}" method="POST" class="col-sm-12" data-parsley-validate>
							{{ csrf_field() }}
							<div class="col-sm-6 form-group">
								<label>First Name</label>
								<input type="text" class="form-control" placeholder="Enter firstname" name="first_name" required>
							</div>
							<div class="col-sm-6 form-group">
								<label>Last Name</label>
								<input type="text" class="form-control" placeholder="Enter Lastname" name="last_name" required>
							</div>
							<div class="col-sm-6 form-group">
								<label>Email</label>
								<input type="email" class="form-control" placeholder="Enter Email" name="email" required>
							</div>
							<div class="col-sm-6 form-group">
								<label>Password</label>
								<input type="password" class="form-control" placeholder="Enter password" name="password" required>
							</div>
							<div class="col-sm-6 form-group">
								<label>Address</label>
								<input type="text" class="form-control" placeholder="Address" name="address" required>
							</div>
							<div class="col-sm-6 form-group">
								<label>Telephone Number</label>
								<input type="number" class="form-control" placeholder="Enter Mobile" name="telephone_number" required>
							</div>
							
							<div class="col-sm-12">
								<button type="submit" class="btn btn-success">
									<i class="fa fa-plus"></i>
								Add Academican</button>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>

	</section> <!-- /.content -->
</div> <!-- /.content-wrapper -->
@endsection

@section('scripts')
<script src="{{ asset('assets/plugins/parsley/parsley.min.js') }}"></script>
@endsection