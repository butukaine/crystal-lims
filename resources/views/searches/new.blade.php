@extends('layouts.master')

@section('content')
  <!-- Content Header (Page header) -->
                <section class="content-header">
                     <form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm" autocomplete="off">
                            <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </form>   
                    <div class="header-icon">
                        <i class="fa fa-search"></i>
                    </div>
                    <div class="header-title">
                        <h1> Searches</h1>
                        <small> New Search</small>
                        <ol class="breadcrumb hidden-xs">
                            <li><a href="index.html"><i class="pe-7s-home"></i> Home</a></li>
                            <li class="active">Search</li>
                        </ol>
                    </div>
                </section>
            <!-- Main content -->
                <section class="content">

                  <div class="row">
<!-- Form controls -->
                            <div class="col-sm-12 " data-lobipanel-child-inner-id="HmSnvsvMGa">
                                <div class="panel panel-bd " data-inner-id="HmSnvsvMGa" data-index="0">
                                    <div class="panel-heading ui-sortable-handle">
                                        <div class="btn-group"> 
                                            <a class="btn btn-primary" href="{{ route('index_search')}}"> <i class="fa fa-list"></i>  Search List </a>  
                                        </div>
                                 </div>
                                    <div class="panel-body">

                                  <form action="{{ route('store_search') }}" method="POST">
    @csrf
    <div class="row">
      <!-- <div class="col-sm-12 form-group">
          <input class="form-control" type="text" name="query" placeholder="Search..." /> 
      </div> -->
      <div class="col-sm-6 form-group">
                  <label><small>NATURE OF SPECIMEN</small></label>

          <select class="form-control form-control select2-selection select2-selection--single"  name="specimen">
            <option selected value="" disabled>Select</option>
            <option value="Histology">Histology</option>
            <option value="Cytology">Cytology</option>
            </select>                                            
      </div>
      <div class="col-sm-6 form-group">
            <label><small>GENDER</small></label>

          <select class="form-control form-control select2-selection select2-selection--single" name="gender">
            <option selected value="" disabled>Select</option>
            <option value="Male">Male</option>
            <option value="Female">Female</option>
            </select>                                            
      </div>
      <div class="col-sm-6 form-group">
          <label><small>NATIONALITY</small></label>
          <input class="form-control" type="text" name="nationality" placeholder="e.g Ugandan" /> 
      </div>
      <div class="col-sm-6 form-group">
          <label><small>AGE RANGE</small></label>
          <div class="row">
            <div class="col-sm-6">
              <input class="form-control col-sm-6" type="number" name="age_start" placeholder="Start" /> 
            </div>
            <div class="col-sm-6">
               <input class="form-control col-sm-6" type="number" name="age_end" placeholder="End" />
            </div>
          </div> 
      </div>
    
      <div class="col-sm-12 form-group ">
          <input type="submit" class="btn btn-l btn-primary" value="Search" />
      </div>
    </div>
</form>
                                     </div>
                                 </div>
                             </div>                         
                         </div>	
                </section> 
            <!-- /.content -->
@endsection
@section('scripts')

            <script type="text/javascript">
            $(function () {
                $('.select2-selection--single').select2();
                $('#datetimepicker1').datetime();
            });
        </script>
@endsection