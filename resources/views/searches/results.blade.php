@extends('layouts.master')

@section('content')

<div class="content" style="margin-top: 20px">
                    <div class="row">
                        <div class="col-sm-12 col-md-12" >
                            <div class="panel panel-bd">
                                <div class="panel-heading">
                                    <div class="panel-title" style="max-width: calc(100% - 180px);">
                                        <h4>Search Results</h4>
                                    </div>
                               </div>
                                <div class="panel-body row">
                                    <!-- <div class="col-sm-6">
                                        <h3>Details</h3>

                                    </div> -->
                                    @foreach($results as $result)
                                    <div class="col-sm-12">                                    
                                        <h2>Result #{{$result->number}}</h2>

                                        <div style="margin-top: 20px">
                                            <label>Pathology Report: </label>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                  <p>{{$result->file}}</p>
                                                </div>
                                                 <div class="col-sm-10">
                                                      <form action="{{route('download')}}" method="GET">
                                                @csrf
                                                <input type="hidden" name="path" value="{{$result->file}}">
                                                <input type="submit" class="btn btn-secondary btn-xs" value="Click here to download">
                                            </form>
                                                </div>
                                            </div>

                                          
                                        </div>
                                        <div>
                                            <label>Biopsy Number: </label>
                                            <p>{{$result->biopsy_no}}</p>
                                            
                                        </div>
                                        
                                        <div>
                                            <label>Comments: </label>
                                            <p>{{$result->comments}}</p>
                                            
                                        </div>
                                        <div>
                                            <label>Date Uploaded: </label>
                                            <p>{{$result->date_uploaded}}</p>
                                            
                                        </div>
                                        <hr>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="panel-footer">
                                    <a href="/searches" class="btn btn-success">Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

@endsection