@extends('layouts.basic')

@section('content')
    <!-- Content Wrapper -->
    <div class="login-wrapper">
           <!--  <div class="back-link">
                <a href="index.html" class="btn btn-success">Back to Dashboard</a>
            </div> -->
        <div class=" lg">
           @if(session()->get('success'))
                                  <div class="alert alert-success alert-dismissible">
                                    {{ session()->get('success') }}  
                                  </div>
                                @endif
            <div class="panel panel-bd">
                <div class="panel-heading">
                    <div class="view-header">
                        <div class="header-icon">
                            <i class="pe-7s-search"></i>
                        </div>
                        <div class="header-title">
                            <h3>Search</h3>
                            <small><strong>Please enter the fields below.</strong></small>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
  <form action="{{ route('store_academician_search') }}" method="POST" autocomplete="off">
    @csrf
    <div class="row">
      <!-- <div class="col-sm-12 form-group">
          <input class="form-control" type="text" name="query" placeholder="Search..." /> 
      </div> -->
       <div class="col-sm-6 form-group">
          <label>DIAGNOSIS/CONCLUSION IN PATHOLOGY REPORT</label>
          <input class="form-control" type="text" name="conclusion" /> 
      </div>
      <div class="col-sm-6 form-group">
                  <label>NATURE OF SPECIMEN</label>

          <select class="form-control form-control select2-selection select2-selection--single"  name="specimen">
            <option selected value="" disabled>Select</option>
            <option value="Histology">Histology</option>
            <option value="Cytology">Cytology</option>
            </select>                                            
      </div>
      <div class="col-sm-6 form-group">
            <label><small>GENDER</small></label>

          <select class="form-control form-control select2-selection select2-selection--single" name="gender">
            <option selected value="" disabled>Select</option>
            <option value="Male">Male</option>
            <option value="Female">Female</option>
            </select>                                            
      </div>
      <div class="col-sm-6 form-group">
          <label>HEALTH UNIT</label>
      <select class="form-control form-control select2-selection select2-selection--single" name="health_facility" placeholder="Select">
          <option selected value="" disabled>Select</option>

              @foreach($health_units as $hu)
          <option value="{{ $hu->name }}">{{ $hu->name }}</option>
              @endforeach
          </select>
      </div>
      <div class="col-sm-6 form-group">
          <label>NATIONALITY</label>
          <input class="form-control" type="text" name="nationality" placeholder="e.g Ugandan" /> 
      </div>
      <div class="col-sm-6 form-group">
          <label>AGE RANGE</label>
          <div class="row">
            <div class="col-sm-6">
              <input class="form-control col-sm-6" type="number" name="age_start" placeholder="Start" /> 
            </div>
            <div class="col-sm-6">
               <input class="form-control col-sm-6" type="number" name="age_end" placeholder="End" />
            </div>
          </div> 
      </div>
         <div class="col-sm-9 form-group">
          <label><small>DATE RANGE</small></label>
          <div class="row">
            
             <div class="col-sm-6 row">
              <div class="col-sm-3">
                <p>From</p>
              </div>
              <div class="col-sm-9">
              <input class="form-control col-sm-6" type="date" name="date_from" placeholder="Start" /> 
              </div>
            </div>
            <div class="col-sm-6 row">
              <div class="col-sm-3">
                <p>To</p>
              </div>
              <div class="col-sm-9">
                <input class="form-control col-sm-6" type="date" name="date_to" placeholder="End" />
              </div>
            </div>
          </div> 
      </div>
      <div class="col-sm-12 form-group ">
          <input type="submit" class="btn btn-l btn-primary" value="Search" />
      </div>
    </div>
</form>
                </div>
            </div>
        </div>
    </div>
    <!-- /.content-wrapper -->
@endsection