@extends('layouts.master')


@section('content')
  <!-- Content Header (Page header) -->
                <section class="content-header">
                     <form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
                            <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-mail-reply"></i></button>
                                </span>
                            </div>
                        </form>   
                    <div class="header-icon">
                        <i class="fa fa-search"></i>
                    </div>
                    <div class="header-title">
                        <h1> Searches</h1>
                        <small> Search Requests List</small>
                        <ol class="breadcrumb hidden-xs">
                            <li><a href="/requests"><i class="pe-7s-home"></i> Home</a></li>
                            <li class="active">Search</li>
                        </ol>
                    </div>
                </section>
            <!-- Main content -->
                <section class="content">
                  <div class="row">
                            <div class="col-sm-12">

                                <div class="panel panel-bd" data-inner-id="a7HgTIWv4t" data-index="0">
                                   <div class="panel-heading">
                                           <div> 
                                            <a class="btn btn-success" href="{{ route('create_search')}}"> <i class="fa fa-plus"></i>  New Search</a>  
                                            
                                           </div>
                                      </div>
                                    <div class="panel-body">
                                      @if(session()->get('success'))
                                  <div class="alert alert-success">
                                    {{ session()->get('success') }}  
                                  </div>
                                @endif
                                @if(session()->get('danger'))
                                  <div class="alert alert-danger">
                                    {{ session()->get('danger') }}  
                                  </div>
                                @endif
                              <div class="table-responsive">
                              <table id="datatable" class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline">  
                                    <thead>
                                        <tr>
                                            <th>REQUESTED BY</th>
                                            <th>DATE REQUESTED</th>
                                            <th>DIAGNOSIS/PATHOLOGY REPORT CONCLUSION</th>
                                            <th>SPECIMEN</th>
                                            <th>GENDER</th>
                                            <th>AGE RANGE</th>
                                            <th>DATE RANGE</th>
                                            <th>HEALTH FACILITY</th>
                                            <th>RESULTS</th>
                                            <th>APPROVED</th>
                                            <th>ACTIONS</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($searches as $search)
                                        <tr>
                                             <td>{{$search->user->name}}</td>
                                             <td>{{$search->created_at->format('j, M Y')}}</td>
                                             <td>{{$search->conclusion}}</td>
                                             <td>{{$search->specimen}}</td>
                                             <td>{{$search->gender}}</td>
                                             <td>{{$search->age_start . " - " . $search->age_end}}</td>
                                             <td>{{$search->date_from . " - " . $search->date_to}}</td>
                                             <td>{{$search->health_facility}}</td>
                                             <td><a href="/get_results/{{$search->id}}"> View({{$search->results_count}})</a></td>
                                         <!--     <td>
                                                @if(1)
                                                  <span class="label-default label label-warning">Responded</span>
                                                @else
                                                  <form method="POST" action="{{route('store_response')}}">
                                                      {{ csrf_field() }}
                                                      <input type="hidden" name="request_id" value="{{$req->id}}" />
                                                      <input type="submit" attr-title="{{$req->id}}"  class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="left" title="Click to submit response" value="Send Response">
                                                  </form>
                                                @endif
                                             </td> -->
                                             <td>
                                               @if($search->approved)
                                                <span class="label-default label label-warning">YES</span>

                                               @else
                                                  <span class="label-default label label-danger">NO</span>

                                               @endif
                                             </td>                                        
                                          
                                            <td>
                                              <!-- <div class="btn-group-vertical" role="group" aria-label="Vertical button group"> -->
                                                <div>
                                                    <a href="/add_results/{{$search->id}}" class="btn btn-default btn-xs">Upload Results</a>
                                                  
                                                    <form method="POST" action="/approve_search" style="margin-top: 8px">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="search_id" value="{{$search->id}}" />
                                                    <input type="submit" attr-title="{{$search->id}}"  class="btn btn-default btn-xs" title="Click to approve" value="Approve Search">
                                                  </form>
                                                  
                                                </div>
                                             </td>
                                        </tr>
                                        @endforeach

                                   
					                </tbody>
					            </table>
        </div>
    </div>
</div>
</div>

</div> 

<div class="modal fade" id="searchModal" tabindex="-1" role="dialog" style="display: none;">
                                    <div class="modal-dialog" role="document">
                                      <form class="col-sm-12" action="{{ route('store_search_result')}}" method="POST" enctype="multipart/form-data">
                                              @csrf
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                <h3 class="modal-title">Upload Result</h3>
                                            </div>
                                             
                                            <div class="modal-body">
                                              <div>
                                             
                                                <input type="hidden" name="search_id" value="10" />
                                                <div class=" form-group col-sm-12">
                                                    <label class="form-label">Upload Documents (Can attach more than one)</label>
                                                    <input class="form-control" type="file" name="files[]" multiple="multiple">
                                                </div>
                                                <div class="col-sm-12 form-group">
                                                    <label>Comment</label>
                                                    <input class="form-control" type="text" name="comments" /> 
                                                </div>
                                            </div>
                                            </div>
                                            <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">CANCEL</button>
                                                    <button type="submit" class="btn btn-success">SAVE</button>
                                            </div>
                                            </div><!-- /.modal-content -->
                                          </form>
                                        </div><!-- /.modal-dialog -->
                                    </div>
</section> 
<!-- /.content -->

<script text="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable({
            columnDefs: [{ orderable: false, targets: [0,10] }],

        });


    });
    function openModal(){
       var radioValue = $("input[name='search_id_check']:checked").val();
       if(radioValue){
            alert("Your are a - " + radioValue);
       }

    }

    function onShowSearchModal(){
        var selected = new Array();
            $("input.search_id_check:checked").each(function() {
                selected.push($(this).val());
            });

            $('.delivery_ids').val('');
            console.log("Selected ids: " + selected.length);
            if(selected.length < 1){
                $('#alert-box').show();
            }else{
                $('.delivery_ids').val(selected.join());
                $('#searchModal').modal('toggle');
                $('#searchModal').modal('show');
            }
        }
</script>
@endsection