@extends('layouts.basic')


@section('content')
            <!-- Main content -->
                <section class="content">
                  <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-bd" data-inner-id="a7HgTIWv4t" data-index="0">
                                   <div class="panel-heading">
                                           <div class="btn-group"> 
                                            <a class="btn btn-success"> <i class="fa fa-list"></i> My Searches</a>  
                                        </div>
                                      </div>
                                    <div class="panel-body">
                              <div class="table-responsive">
                              <table class="table table-striped table-hover dataTable dtr-inline">  
                                    <thead>
                                        <tr>
                                            <th>DATE REQUESTED</th>
                                            <th>DIAGNOSIS/CONCLUSION IN PATHOLOGY REPORT</th>
                                            <!-- <th>SPECIMEN</th>
                                            <th>GENDER</th>
                                            <th>AGE RANGE</th> -->
                                            <th>APPROVED</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($searches as $search)
                                        <tr>
                                             <td>{{$search->created_at->format('j, M Y')}}</td>
                                             <td>{{$search->conclusion}}</td>
                                             <!-- 
                                             <td>{{$search->specimen}}</td>
                                             <td>{{$search->gender}}</td>
                                             <td>{{$search->age_start . " to " . $search->age_end}}</td> -->
                                         <!--     <td>
                                                @if(1)
                                                  <span class="label-default label label-warning">Responded</span>
                                                @else
                                                  <form method="POST" action="{{route('store_response')}}">
                                                      {{ csrf_field() }}
                                                      <input type="hidden" name="request_id" value="{{$req->id}}" />
                                                      <input type="submit" attr-title="{{$req->id}}"  class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="left" title="Click to submit response" value="Send Response">
                                                  </form>
                                                @endif
                                             </td> -->
                                             
                                               @if($search->approved)
                                                <td><span class="label-default label label-warning">YES</span></td>
                                                <td>

                                                    <a type="submit" attr-title="{{$search->id}}" href="/myresults/{{$search->id}}" class="btn btn-purple btn btn-s" data-toggle="tooltip" data-placement="left" title="Click to see results"> See Results</a>
                                                 
                                                </td>
                                               @else
                                                  <td><span class="label-default label label-danger">NO</span></td>
                                                  <td>
                                                    <a type="submit" attr-title="{{$search->id}}"  class="btn btn-purple disabled btn btn-s" data-toggle="tooltip" data-placement="left" title="Click to see results">No Results</a>
                                                  </td>
                                               @endif
                                             
                                        </tr>
                                        @endforeach
                                   
					                </tbody>
					            </table>
        </div>
    </div>
</div>
</div>
</div> 
</section> 
<!-- /.content -->

@endsection