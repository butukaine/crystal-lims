
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Mainstream Biomedical Centre - Admin Dashboard</title>

    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="/favicon.png" type="image/x-icon">

    <!-- Bootstrap -->
    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Bootstrap rtl -->
    <!--<link href="assets/bootstrap-rtl/bootstrap-rtl.min.css" rel="stylesheet" type="text/css"/>-->
    <!-- Theme style -->
    <link href="assets/dist/css/stylehealth.min.css" rel="stylesheet" type="text/css"/>
    <!-- Theme style rtl -->
    <!--<link href="assets/dist/css/stylehealth-rtl.css" rel="stylesheet" type="text/css"/>-->
</head>
<body>
    <div class="middle-box2">
        
<div class="row">
<div class="col-sm-12">
<strong>

<h3 class="text-center"><strong>MAINSTREAM BIOMEDICAL CENTRE USER AGREEMENT</strong></h3><br>
<p align="justify">
The laws of Uganda on Data Protection and Privacy Act, 2019 and Industrial Property Act, 2014 shall apply. 
<br><br>
1.1 Definition of Confidential Information. As used herein, "Confidential Information" includes all information given to the “Customer” (the registered "Clinician", “Health Facility” or “Academician”) or its affiliates by the “Supplier” (the "Mainstream Biomedical Centre") or its affiliates or otherwise acquired by the Customer or its affiliates, in connection with this Agreement, and all information derived or generated therefrom, including (a) information regarding prices, costs, know-how information, technology of “Supplier” and (b) this Agreement.
<br><br>
1.2 Confidentiality Agreement:
During the course of preparing and providing Products and Services, “Customer” will be provided with information concerning Mainstream Biomedical Centre including, without limitation, information, techniques, or know-how, that is confidential and the disclosure of which would cause irreparable injury to Products and Services (collectively, the "Confidential Information") of “Supplier”.  “Customer” agrees not to disclose the Confidential Information to any person unless “Customer” has received prior written authorization from “Supplier” of Mainstream Biomedical Centre.  Additionally, upon termination or expiration of this Agreement for any reason or upon the request of Mainstream Biomedical Centre, “Customer” shall promptly commit to confidential storage of all originals and copies of documents or other materials constituting or containing Confidential Information in accordance with the Uganda Data Protection and Privacy Act, 2019, Industrial Property Act, 2014 and any Uganda Ministry of Health standards for healthcare information protection.  
<br><br>
1.3 Intellectual Property Rights:
Restrictions on Use and Disclosure. The “Customer” shall not, except as otherwise provided below (a) use or reproduce the “Supplier” Confidential Information for any purpose other than as required in connection with this Agreement or (b) disclose the “Supplier” Confidential Information to any third party, without the prior written approval of the “Supplier”, except to personnel, consultants, agents and representatives of the “Customer” or its affiliates who have a need to know such information in connection with the Services; provided the “Customer” shall be responsible for any actions of such parties that would be in breach of this Agreement if done by the “Customer”. The “Customer” may disclose the “Supplier” Confidential Information to the extent such information is required to be disclosed by law, including a subpoena, or to respond to a regulatory request; provided that the “Customer” promptly notifies the “Supplier” in writing prior to any disclosure to allow the “Supplier” to seek a protective order or similar relief at “Supplier” sole discretion.
<br><br>
1.4 Exceptions. The “Customer” has no obligation to protect the following categories of “Supplier”  information: (a) information that is or was independently developed by the Customer without use of or reference to any of the “Supplier” Confidential Information, (b) information that is or was lawfully received from a third party without any restriction on use; or (c) information that becomes or was a part of the public domain through no breach of the Uganda Industrial Property Act, 2014 and the Uganda Data Protection and Privacy Act, 2019.
</p>
</strong>
</div>
        </div>
        <!-- jQuery -->
        <script src="assets/plugins/jQuery/jquery-1.12.4.min.js" type="text/javascript"></script>
        <!-- bootstrap js-->
        <script src="assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
    </html>