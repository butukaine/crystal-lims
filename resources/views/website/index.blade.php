<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Welcome | {{ config('app.name')}}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.png">

    <!-- <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,,500,600,700" rel="stylesheet"> -->
     <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">


    <link rel="stylesheet" href="/website/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="/website/css/animate.css">
    
    <link rel="stylesheet" href="/website/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/website/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="/website/css/magnific-popup.css">

    <link rel="stylesheet" href="/website/css/aos.css">

    <link rel="stylesheet" href="/website/css/ionicons.min.css">

    <link rel="stylesheet" href="/website/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="/website/css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="/website/css/flaticon.css">
    <link rel="stylesheet" href="/website/css/icomoon.css">
    <link rel="stylesheet" href="/website/css/style.css">
  </head>
  <body>
    
	  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
	   <a  class="navbar-brand text-center"href="{{route('landing_page')}}"><img style="height:80px" alt="logo" src="logo-white.png" />
                        
                    </a>
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>

	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav ml-auto">
	         <!--  <li class="nav-item active"><a href="index.html" class="nav-link">Home</a></li>
	          <li class="nav-item"><a href="about.html" class="nav-link">About</a></li>
	          <li class="nav-item"><a href="domain.html" class="nav-link">Domain</a></li>
	          <li class="nav-item"><a class="nav-link" href="hosting.html">Hosting</a></li>
	          <li class="nav-item"><a href="blog.html" class="nav-link">Blog</a></li>
	          <li class="nav-item"><a href="contact.html" class="nav-link">Contact</a></li> -->
	          <li class="nav-item cta"><a href="{{route('login')}}" class="nav-link"><span>TRY DEMO</span></a></li>
	        </ul>
	      </div>
	    </div>
	  </nav>
    <!-- END nav -->

    <section class="home-slider owl-carousel">
      <div class="slider-item" style="background-image: url(/website/images/bg_1.jpg);">
        <div class="overlay"></div>
        <div class="container-fluid">
          <div class="row slider-text align-items-center" data-scrollax-parent="true">

            <div class="col-md-5 wrap col-sm-12 ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
              <h1 class="mb-4 mt-5" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">Electronic Health System</h1>
              <p class="mb-4 mb-md-5 sub-p" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">Manage all your daily tasks in one place and track the records of your patients effectively</p>
              <p><a href="{{route('login')}}" class="btn btn-primary p-3 px-xl-5 py-xl-3">Get started</a></p>
            </div>
            <div class="col-md-7 ftco-animate">
            	<img src="/website/images/Laptop1.png" class="img-fluid" alt="">
            </div>

          </div>
        </div>
      </div>

     <!--  <div class="slider-item" style="background-image: url(/website/images/bg_2.jpg);">
        <div class="overlay"></div>
        <div class="container-fluid">
          <div class="row slider-text align-items-center" data-scrollax-parent="true">

            <div class="col-md-5 wrap col-sm-12 ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
              <h1 class="mb-4 mt-5" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">Electronic Health System</h1>
              <p class="mb-4 mb-md-5">Track the records of your patients effectively</p>
              <p><a href="{{route('login')}}" class="btn btn-primary p-3 px-xl-5 py-xl-3">Get started</a></p>
            </div>
            <div class="col-md-7 ftco-animate">
            	<img src="/website/images/dashboard_full_3.png" class="img-fluid" alt="">
            </div>

          </div>
        </div>
      </div> -->
    </section>
    
  <!--   <section class="ftco-domain">
    	<div class="container">
    		<div class="row d-flex">
    			<div class="col-lg-5 heading-white mb-4 mb-sm-4 mb-lg-0 ftco-animate">
    				<h2>Search You Domain Name</h2>
    				<p>A small river named Duden flows by their place</p>
    			</div>
    			<div class="col-lg-7 ftco-wrap ftco-animate">
    				<form action="#" class="domain-form d-flex">
              <div class="form-group domain-name">
                <input type="text" class="form-control name px-4" placeholder="Enter your domain name...">
              </div>
              <div class="form-group domain-select d-flex">
	              <div class="select-wrap">
                  <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                  <select name="" id="" class="form-control">
                  	<option value="">.com</option>
                    <option value="">.net</option>
                    <option value="">.biz</option>
                    <option value="">.co</option>
                    <option value="">.me</option>
                  </select>
                </div>
                <input type="submit" class="search-domain btn btn-primary text-center" value="Search">
	            </div>
            </form>
            <p class="domain-price mt-2"><span><small>.com</small> $9.75</span> <span><small>.net</small> $9.50</span> <span><small>.biz</small> $8.95</span> <span><small>.co</small> $7.80</span><span><small>.me</small> $7.95</span></p>
    			</div>
    		</div>
    	</div>
    </section>
   -->
    <section class="ftco-section services-section bg-light">
      <div class="container">
      	<div class="row justify-content-center mb-5 pb-3">
          <div class="col-md-7 text-center heading-section ftco-animate">
            <h2 class="mb-4">The Main Features</h2>
            <p>Our EHS offers you the following functionalities</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services d-block text-center">
              <div class="d-flex justify-content-center">
              	<div class="icon d-flex align-items-center justify-content-center">
              		<span class="flaticon-guarantee"></span>
              	</div>
              </div>
              <div class="media-body p-2 mt-3">
                <h3 class="heading">Cases Management</h3>
                <p>An inventory of patient cases with a highly responsive layout.Provision for case searching and creation.</p>
              </div>
            </div>      
          </div>
          <div class="col-md-4 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services d-block text-center">
              <div class="d-flex justify-content-center">
              	<div class="icon d-flex align-items-center justify-content-center">
              		<span class="flaticon-shield"></span>
              	</div>
              </div>
              <div class="media-body p-2 mt-3">
                <h3 class="heading">Medical Reports Generation</h3>
                <p>Reports can be generated specifically and can be automatically saved to file.</p>
              </div>
            </div>    
          </div>
          <div class="col-md-4 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services d-block text-center">
              <div class="d-flex justify-content-center">
              	<div class="icon d-flex align-items-center justify-content-center">
              		<span class="flaticon-support"></span>
              	</div>
              </div>
              <div class="media-body p-2 mt-3">
                <h3 class="heading">User Authentication</h3>
                <p>New users create accounts and login after approval from the admin.</p>
              
                </div>
            </div>      
          </div>
					<div class="col-md-4 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services d-block text-center">
              <div class="d-flex justify-content-center">
              	<div class="icon d-flex align-items-center justify-content-center">
              		<span class="flaticon-cloud-computing"></span>
              	</div>
              </div>
              <div class="media-body p-2 mt-3">
                <h3 class="heading">Email Notifications</h3>
                <p>Automated emails sent to users for high priority notifications</p>
                </div>
            </div>      
          </div>
          <div class="col-md-4 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services d-block text-center">
              <div class="d-flex justify-content-center">
              	<div class="icon d-flex align-items-center justify-content-center">
              		<span class="flaticon-settings"></span>
              	</div>
              </div>
              <div class="media-body p-2 mt-3">
                <h3 class="heading">Data Analysis</h3>
                <p>Using historical data, the interface can create a case analysis bar chart.And further analysis that can identify performace.</p>
              </div>
            </div>    
          </div>
          <div class="col-md-4 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services d-block text-center">
              <div class="d-flex justify-content-center">
              	<div class="icon d-flex align-items-center justify-content-center">
              		<span class="flaticon-loading"></span>
              	</div>
              </div>
              <div class="media-body p-2 mt-3">
                <h3 class="heading">Workflow Management</h3>
                <p>Elimination of unnecessary workflow steps and clicks through the system.</p>
              
              </div>
            </div>      
          </div>
        </div>
      </div>
    </section>

  <!--   <section class="ftco-section ftco-counter img" id="section-counter" style="background-image: url(/website/images/bg_1.jpg);" data-stellar-background-ratio="0.5">
    	<div class="container">
    		<div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
            <span class="subheading">More than 100,000 websites hosted</span>
          </div>
        </div>
    		<div class="row justify-content-center">
    			<div class="col-md-10">
		    		<div class="row">
		          <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
		            <div class="block-18 text-center">
		              <div class="text">
		                <strong class="number" data-number="2000">0</strong>
		                <span>CMS Installation</span>
		              </div>
		            </div>
		          </div>
		          <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
		            <div class="block-18 text-center">
		              <div class="text">
		                <strong class="number" data-number="100">0</strong>
		                <span>Awards Won</span>
		              </div>
		            </div>
		          </div>
		          <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
		            <div class="block-18 text-center">
		              <div class="text">
		                <strong class="number" data-number="32000">0</strong>
		                <span>Registered Domains</span>
		              </div>
		            </div>
		          </div>
		          <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
		            <div class="block-18 text-center">
		              <div class="text">
		                <strong class="number" data-number="31998">0</strong>
		                <span>Satisfied Customers</span>
		              </div>
		            </div>
		          </div>
		        </div>
	        </div>
        </div>
    	</div>
    </section> -->

	 <section class="ftco-services">
      <div class="container-wrap">
        <div class="row no-gutters">
          <div class="col-lg-5 img " style="background-image: url(/website/images/side-dash.png);">
            
            </a>
          </div>
          <div class="col-lg-7">
            <div class="services-wrap p-4 p-md-5">
              <div class="heading-section mb-5 ftco-animate">
                <h2 class="mb-2">Benefits of our EHS</h2>
                <span class="subheading">Modern system for your laboratory</span>
              </div>
              <div class="d-md-flex">
                <div class="one-half mr-4">
                  <div class="list-services d-flex ftco-animate">
                    <div class="icon d-flex order-md-last justify-content-center align-items-center">
                      <span class="flaticon-cloud-computing"></span>
                    </div>
                    <div class="text pl-4 pl-sm-0 pr-md-4 text-md-right">
                      <h3>Easy-to-use</h3>
                      <p>User friendly system that aids their daily routines</p>
                    </div>
                  </div>
                  <div class="list-services d-flex ftco-animate">
                    <div class="icon d-flex order-md-last justify-content-center align-items-center">
                      <span class="flaticon-bandwidth"></span>
                    </div>
                    <div class="text pl-4 pl-sm-0 pr-md-4 text-md-right">
                      <h3>Efficiency</h3>
                      <p>Faster, more efficient and more productive working</p>
                    </div>
                  </div>
                  <div class="list-services d-flex ftco-animate">
                    <div class="icon d-flex order-md-last justify-content-center align-items-center">
                      <span class="flaticon-shield"></span>
                    </div>
                    <div class="text pl-4 pl-sm-0 pr-md-4 text-md-right">
                      <h3>Security</h3>
                      <p>Prevents unauthorized access to the system and patient records</p>
                    </div>
                  </div>
                </div>

                <div class="one-half">
                  <div class="list-services d-flex ftco-animate">
                    <div class="icon d-flex justify-content-center align-items-center">
                      <span class="flaticon-guarantee"></span>
                    </div>
                    <div class="text pl-4 pl-sm-0 pl-md-4">
                      <h3>Storage</h3>
                      <p>Reliable storage for your important documents </p>
                    </div>
                  </div>
                  <div class="list-services d-flex ftco-animate">
                    <div class="icon d-flex justify-content-center align-items-center">
                      <span class="flaticon-settings"></span>
                    </div>
                    <div class="text pl-4 pl-sm-0 pl-md-4">
                      <h3>Customizable</h3>
                      <p>Adjustable to meet customers specific needs</p>
                    </div>
                  </div>
                  <div class="list-services d-flex ftco-animate">
                    <div class="icon d-flex justify-content-center align-items-center">
                      <span class="flaticon-support"></span>
                    </div>
                    <div class="text pl-4 pl-sm-0 pl-md-4">
                      <h3>Tech Support</h3>
                      <p>You can contact us incase of any challenges.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

<!--     <section class="ftco-section testimony-section">
      <div class="container">
        <div class="row justify-content-center mb-5 pb-3">
          <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
            <h2 class="mb-4">SCREENSHOTS</h2>
            <p>Explore some of the features of the system</p>
          </div>
        </div>
        <div class="row ftco-animate">
          <div class="col-md-12">
            <div class="carousel-testimony owl-carousel ftco-owl">
              <div class="item">
                <div class="testimony-wrap p-4 pb-5">
                  <img src="/website/images/register.jpg">

                 
                </div>
              </div>
              <div class="item">
                <div class="testimony-wrap p-4 pb-5">
                  <img src="/website/images/ccreate.png">

                 
                </div>
              </div>
              <div class="item">
                <div class="testimony-wrap p-4 pb-5">
                  <img src="/website/images/Fnewcase.png">

                 
                </div>
              </div>
              <div class="item">
                <div class="testimony-wrap p-4 pb-5">
                  <img src="/website/images/request.png">

                 
                </div>
              </div>
              <div class="item">
                <div class="testimony-wrap p-4 pb-5">
                  <img src="/website/images/loogin.jpg">

                 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section> -->

    <section class="ftco-section">
    	<div class="container">
    		<div class="row justify-content-center mb-5 pb-5">
    			<div class="col-md-7 text-center heading-section ftco-animate">
<!--             <span class="subheading">Services</span>
 -->            <h2 class="mb-4">How it works</h2>
          </div>
    		</div>
    		<div class="row">
          <div class="col-md-12 nav-link-wrap mb-5 pb-md-5 pb-sm-1 ftco-animate">
            <div class="nav ftco-animate nav-pills justify-content-center text-center" id="v-pills-tab" role="tablist" aria-orientation="vertical">
              <a class="nav-link active" id="v-pills-nextgen-tab" data-toggle="pill" href="#v-pills-nextgen" role="tab" aria-controls="v-pills-nextgen" aria-selected="true">Create Case</a>

              <a class="nav-link" id="v-pills-performance-tab" data-toggle="pill" href="#v-pills-performance" role="tab" aria-controls="v-pills-performance" aria-selected="false">Submit Request</a>

              <a class="nav-link" id="v-pills-effect-tab" data-toggle="pill" href="#v-pills-effect" role="tab" aria-controls="v-pills-effect" aria-selected="false">Prepare Report</a>
            </div>
          </div>
          <div class="col-md-12 align-items-center ftco-animate">
            
            <div class="tab-content ftco-animate" id="v-pills-tabContent">

              <div class="tab-pane fade show active" id="v-pills-nextgen" role="tabpanel" aria-labelledby="v-pills-nextgen-tab">
              	<div class="d-md-flex">
	              	<div class="one-forth align-self-center">
	              		<img src="/website/images/new_case.png" class="img-fluid border" alt="">
	              	</div>
	              	<div class="one-half ml-md-5 align-self-center">
		                <h2 class="mb-4">Create Case</h2>
		              	<p>A clinician can create a Patient case, with a single click of a button. creating a case is a data collection process where all the useful data in the case form can then be saved.</p>
		              </div>
	              </div>
              </div>

              <div class="tab-pane fade" id="v-pills-performance" role="tabpanel" aria-labelledby="v-pills-performance-tab">
                <div class="d-md-flex">
	              	<div class="one-forth order-last align-self-center">
	              		<img src="/website/images/ccreate.png" class="img-fluid border" alt="">
	              	</div>
	              	<div class="one-half order-first mr-md-5 align-self-center">
		                <h2 class="mb-4">Submit Request</h2>
		              	<p>When a new case is created, it appears among the list of cases. From here, a request  can be submitted to the list of requests awaiting the respective pathologist to prepare a report.</p>
		              </div>
	              </div>
              </div>

              <div class="tab-pane fade" id="v-pills-effect" role="tabpanel" aria-labelledby="v-pills-effect-tab">
                <div class="d-md-flex">
	              	<div class="one-forth align-self-center">
	              		<img src="/website/images/prepare_report.png" class="img-fluid border" alt="">
	              	</div>
	              	<div class="one-half ml-md-5 align-self-center">
		                <h2 class="mb-4">Prepare Report</h2>
		              	<p>The process of report preparation captures the patient details, physician details and pathology report.</p>
                    <p> The pathologist recieves a list of requests to which he attends and prepares a report for each request.</p>
		              </div>
	              </div>
              </div>
            </div>
          </div>
        </div>
    	</div>
    </section>

    <footer class="ftco-footer ftco-bg-dark ftco-section">
      <div class="container">
       
        <div class="row">
          <div class="col-md-12 text-center">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Powered By <a href="http://crystaltechnologiesug.com/" target="_blank">Crystal Technologies</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
          </div>
        </div>
      </div>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="/website/js/jquery.min.js"></script>
  <script src="/website/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="/website/js/popper.min.js"></script>
  <script src="/website/js/bootstrap.min.js"></script>
  <script src="/website/js/jquery.easing.1.3.js"></script>
  <script src="/website/js/jquery.waypoints.min.js"></script>
  <script src="/website/js/jquery.stellar.min.js"></script>
  <script src="/website/js/owl.carousel.min.js"></script>
  <script src="/website/js/jquery.magnific-popup.min.js"></script>
  <script src="/website/js/aos.js"></script>
  <script src="/website/js/jquery.animateNumber.min.js"></script>
  <script src="/website/js/bootstrap-datepicker.js"></script>
  <script src="/website/js/jquery.timepicker.min.js"></script>
  <script src="/website/js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="/website/js/google-map.js"></script>
  <script src="/website/js/main.js"></script>
    
  </body>
</html>