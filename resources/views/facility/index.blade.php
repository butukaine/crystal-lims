@extends('test.test')
    
@section('content')

<div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Health Facilities</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <a class="btn btn-success" href="{{ route('create_health_facility') }}"> <i class="fa fa-plus"></i>
                                      Add Health Facility </a>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <div class="table-responsive">
                                            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                            
                                            
                                            <div class="row"><div class="col-sm-12">
                                            <table class="table table-striped table-bordered zero-configuration dataTable" id="datatable" role="grid" aria-describedby="DataTables_Table_0_info">
                                                <thead>
                                                    <tr role="row">
                                                    <th>#</th
                                                    ><th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 158.333px;">FACILITY NAME</th>
                                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 76.3333px;">LOCATION</th>
                                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 27.6667px;">DISTRICT</th>
                                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width:
                                                        27.6667px;">ADDED AT</th>
                                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width:
                                                        27.6667px;">ACTION</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                              @forelse ($health_facilities as $health_facility)
                                    <tr >
                                        <td>
                                            <label>{{ $health_facility->id }}</label>   
                                        </td>
                                        <td>{{ $health_facility->name }}</td>
                                        <td>{{ $health_facility->location }}</td>
                                        <td>{{ $health_facility->district }}</td>
                                        <td>{{ $health_facility->created_at->diffForHumans() }}</td>
                                        <td>
                                            <button class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="left" title="Update"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</button>
                                        </td>
                                    </tr>
                                    @empty
                                    
                                    @endforelse
                                                    
                                                    
                                                    
                                                </tbody>
                                               
                                            </table></div></div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <script>
                    $('#datatable').DataTable( {
                        responsive: true
                    } );
                    </script>


                                        
@endsection


