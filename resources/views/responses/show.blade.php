@extends('layouts.master')
    
@section('content')
<div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">{{session('title')}}</h3>
                </div>
                <div class="content-header-right col-md-8 col-12">
                    <div class="breadcrumbs-top float-md-right">
                        <div class="breadcrumb-wrapper mr-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('index_responses')}}">Reports</a>
                                </li>
                                <li class="breadcrumb-item active">{{session('title')}}
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>



<div class="content-body">
                <!-- Zero configuration table -->
                <section id="configuration">
<div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Report details</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <a type="button" class="btn btn-outline-secondary" href="{{ route('download_response',  $response->id)}}">
                                <i class="ft-arrow-up-right"></i>
                                Export
                            </a>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard row">
                                     <div class="col-sm-6">
                                                <div class="form-group text-center ">
                                                    <label class="text-success"><h2><b>PATIENT DETAILS</b></h2></label>
                                                </div>
                                            <div class="form-group row">
                                                    <div class="col-sm-4">
                                                        <label><b>NAME OF PATIENT:</b></label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <p> {{$case->patient_name}}</p>          
                                                    </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <div class="col-sm-4">
                                                    <label><b>PATIENT'S I.D NUMBER:</b></label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                    <p>{{$case->patient_number}}</p>
                                                    </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-4">
                                                    <label><b>GENDER:</b></label><br>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <p>{{$case->gender}}</p>
                                                        
                                                    </div>
                                            </div>
                                             <div class="form-group row">
                                                <div class="col-sm-4">
                                                        <label><b>AGE:</b></label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        @if($case->age != null)
                                                      <p>{{$case->age}}</p>
                                                      @endif

                                                    </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-4">
                                                    <label><b>VILLAGE/ZONE:</b></label>
  
                                                    </div>
                                                    <div class="col-sm-8">
                                                    <p>{{$case->village}}</p>

                                                    </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-4">
                                                    <label><b>PARISH:</b></label>
 
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <p>{{$case->parish}}</p>
                                                    </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <div class="col-sm-4">
                                                 <label><b>SUB-COUNTY:</b></label>                                                     
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <p>{{$case->sub_county}}</p>
                                                    </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-4">
                                                <label><b>DISTRICT:</b></label>                                                        
                                                    </div>
                                                    <div class="col-sm-8">
                                                <p>{{$case->district}}</p>
                                                    </div>
                                            </div>
                                    </div>
                                            <div class="col-sm-6">
                                                <div class="form-group text-center ">
                                                    <label class="text-success"><h2><b>PHYSICIAN DETAILS</b></h2></label>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-4">
                                                        <label><b>NAME OF PHYSICIAN:</b></label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        
                                                        <p>{{$response->physician_name}}</p>
                                                    </div>
                                                </div>
                                            
                                                <div class="form-group row">
                                                    <div class="col-sm-4">
                                                        <label><b>CONTACT ADDRESS:</b></label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <p>{{$response->physician_contact}}</p>
                                                        </div>
                                                </div>
                                                 <div class="form-group row">
                                                    <div class="col-sm-4">
                                                        <label><b>TELEPHONE:</b></label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <p>{{$response->physician_mobile}}</p>
                                                                  
                                                    </div>
                                                </div>
                                            
                                                <div class="form-group row">
                                                    <div class="col-sm-4">
                                                        <label><b>NATURE OF SPECIMEN:</b></label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <p>{{$case->specimen}}</p>
                                                            
                                                        </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-4">
                                                        <label><b>DATE OF RECEIPT:</b></label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <p>{{$response->date_of_receipt}}</p>
                                                                   
                                                    </div>
                                                </div>
                                                 <div class="form-group row">
                                                    <div class="col-sm-4">
                                                        <label><b>HEALTH UNIT:</b></label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <p>{{$response->health_unit}}</p>
                                                                   
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-4">
                                                        <label><b>BIOPSY NO.:</b></label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <p>{{$response->biopsy_no}}</p>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-sm-4">
                                                        <label><b>SUBMISSION DATE:</b></label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <p>{{$response->submitted_at}}</p>
                                                                   
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-12 form-group text-center ">
                                                <label class="text-success"><h2><b>PATHOLOGY REPORT</b></h2></label>
                                            </div>
                                    
                                            <div class="col-sm-6 form-group">
                                                <label><b>CLINICAL SUMMARY:</b></label>
                                                <p>{{$response->clinical_summary}}</p>
                                                
                                            </div> 
                                            <div class="col-sm-6 form-group">
                                                <label><b>MACROSCOPIC APPEARANCE:</b></label>
                                                <p>{{$response->ma_appearance}}</p>
                                            </div> 
                                            <div class="col-sm-6 form-group">
                                                <label><b>MICROSCOPIC APPEARANCE:</b></label>
                                                <p>{{$response->mi_appearance}}</p>

                                            </div>  
                                            <div class="col-sm-6 form-group">
                                                <label><b>CONCLUSION:</b></label>
                                                <p>{{$response->conclusion}}</p>

                                            </div>
                                            
                                             <div class="col-sm-6 form-group">
                                                    <div class="col-sm-4">
                                                        <label><b>ATTACHED DOCUMENT:</b></label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        @if($response->document != "")
                                                        <form action="{{route('download')}}" method="GET">
                                                            @csrf
                                                            <input type="hidden" name="path" value="{{$response->document}}">
                                                            <input type="submit" class="btn btn-primary btn-xs" value="Download">
                                                        </form>
                                                        @else
                                                            <p>No file available</p>
                                                        @endif
                                                        <!-- <input type="file" name="report_doc" required> -->
                                                        
                                                    </div>
                                                </div>  
                                            <div class="col-sm-6 form-group">
                                                <label><b>COMMENTS:</b></label>
                                                <p>{{$response->comments}}</p>

                                            </div>
                                            <div class="form-group col-sm-6 row">
                                                <div class="col-sm-4">
                                                    <label><b>PATHOLOGIST'S NAME:</b></label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <p>{{$response->pathologist_name}}</p>
                                                               
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-6" style="margin-left: 10px;">
                                                <div class="col-sm-4">
                                                    <label><b>DATE OF REPORT:</b></label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <p>{{$response->date_of_report}}</p>
                                                               
                                                </div>
                                            </div>
                                               <div class="col-sm-12" style="padding:30px; background: #374767; color: white; margin-top: 30px;">
                                                    <b>Important Note:</b> The original handwritten signature and date of report by Pathologist are not hosted in the Electronic Health Record System. They are available as hard copy at the laboratory or as scanned copies upon request.
                                                    
                                                </div>  


                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </section>
              </div>


                                        
@endsection


