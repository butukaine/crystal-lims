<head>
  <style type="text/css">
    table, th, td {
    border: 0.5px solid green;
      border-collapse: collapse;
}
th, td {
  padding: 5px;
}
table {
  border-spacing: 5px;
}
  </style>
</head>
  <body>
    <div>
      <img class="text-center" alt="Logo" src="{{public_path('/logo-red.png')}}"
                     width=230" height="100"> 
          <h1>List of Responses</h1>
          <p>Download Date: {{$date}}</p>
          <hr>
    </div>
  <div class="table-responsive" style="padding-top: 15px;">
                                <table id="datatable" class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline">  
                                    <thead>
                                        <tr>
                                           <th>REQUEST NO</th>
                                            <th>BIOPSY NO</th>
                                            <th>CLINICAL SUMMARY</th>
                                            <th>PATHOLOGIST NAME</th>
                                            <th>DATE OF REPORT</th>
                                            <th>DATE CREATED</th>
                                            <th>STATUS</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                       @foreach($responses as $resp)
                                        <tr>
                                             <td>Request {{$resp->request_id}}</td>
                                             <td>{{$resp->biopsy_no}}</td>
                                             <td>{{$resp->clinical_summary}}</td>
                                             <td>{{$resp->pathologist_name}}</td>
                                             <td>{{$resp->date_of_report}}</td>
                                             <td>{{$resp->created_at->format('j, M Y')}}</td>
                                             <td>
                                               @if($resp->submitted)
                                                  <span class="label-default label label-warning">Submitted</span>
                                                @else
                                                  <span class="label-default label label-warning" style="color:red;">Pending Submission</span>

                                                @endif
                                             </td>
                                        </tr>
                                        @endforeach
                                   
                </tbody>
            </table>
        </div>
  </body>
