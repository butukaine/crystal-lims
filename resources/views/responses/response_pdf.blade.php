  
                                    <div class="panel-body">
                                        <img class="text-center" alt="Logo" src="{{public_path('/logo-red.png')}}"
                     width=230" height="100"> 
                                <h1>Response#{{$response->id}}</h1>
                                <p><strong>BIOPSY NUMBER:</strong>{{$response->biopsy_no}}</p>

                                           <hr>
                                           
                                            <div class="col-sm-6">
                                                <div class="form-group text-center ">
                                                    <strong class="text-success"><h2 style="color: green">PATIENT DETAILS</h2></strong>
                                                </div>
                                            <div class="form-group row">
                                                    <div class="col-sm-4">
                                                        <strong>NAME OF PATIENT:</strong>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <p> {{$case->patient_name}}</p>          
                                                    </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <div class="col-sm-4">
                                                    <strong>PATIENT'S I.D NUMBER:</strong>
                                                    </div>
                                                    <div class="col-sm-8">
                                                	<p>{{$case->patient_number}}</p>
                                                    </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-4">
                                                    <strong>GENDER:</strong><br>
                                                    </div>
                                                    <div class="col-sm-8">
                                                    	<p>{{$case->gender}}</p>
                                                        
                                                    </div>
                                            </div>
                                             <div class="form-group row">
                                                <div class="col-sm-4">
                                                        <strong>AGE:</strong>
                                                    </div>
                                                    <div class="col-sm-8">
                                                      <p>{{$case->age}}</p>

                                                    </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-4">
                                                    <strong>VILLAGE/ZONE:</strong>
  
                                                    </div>
                                                    <div class="col-sm-8">
                                                	<p>{{$case->village}}</p>

                                                    </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-4">
                                                    <strong>PARISH:</strong>
 
                                                    </div>
                                                    <div class="col-sm-8">
                                                		<p>{{$case->parish}}</p>
                                                    </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <div class="col-sm-4">
                                                 <strong>SUB-COUNTY:</strong>                                                     
                                                    </div>
                                                    <div class="col-sm-8">
                                                		<p>{{$case->sub_county}}</p>
                                                    </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-4">
                                                <strong>DISTRICT:</strong>                                                        
                                                    </div>
                                                    <div class="col-sm-8">
                                                <p>{{$case->district}}</p>
                                                    </div>
                                            </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group text-center ">
                                                    <strong class="text-success"><h2 style="color: green">PHYSICIAN DETAILS</h2></strong>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-4">
                                                        <strong>NAME OF PHYSICIAN:</strong>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        
                                                        <p>{{$response->physician_name}}</p>
                                                    </div>
                                                </div>
                                            
                                                <div class="form-group row">
                                                    <div class="col-sm-4">
                                                        <strong>CONTACT ADDRESS:</strong>
                                                        </div>
                                                        <div class="col-sm-8">
                                                        	<p>{{$response->physician_contact}}</p>
                                                        </div>
                                                </div>
                                                 <div class="form-group row">
                                                    <div class="col-sm-4">
                                                        <strong>TELEPHONE:</strong>
                                                    </div>
                                                    <div class="col-sm-8">
                                                    	<p>{{$response->physician_mobile}}</p>
                                                                  
                                                    </div>
                                                </div>
                                            
                                                <div class="form-group row">
                                                    <div class="col-sm-4">
                                                        <strong>NATURE OF SPECIMEN:</strong>
                                                        </div>
                                                        <div class="col-sm-8">
                                                        	<p>{{$case->specimen}}</p>
                                                            
                                                        </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-4">
                                                        <strong>DATE OF RECEIPT:</strong>
                                                    </div>
                                                    <div class="col-sm-8">
                                                    	<p>{{$response->date_of_receipt}}</p>
                                                                   
                                                    </div>
                                                </div>
                                                 <div class="form-group row">
                                                    <div class="col-sm-4">
                                                        <strong>HAELTH UNIT:</strong>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <p>{{$response->health_unit}}</p>
                                                                   
                                                    </div>
                                                </div>
                                             
                                            </div>

                                            <div class="col-sm-12 form-group text-center ">
                                                <strong class="text-success"><h2 style="color: green">PATHOLOGY REPORT</h2></strong>
                                            </div>
                                    
                                            <div class="col-sm-6 form-group">
                                                <strong>CLINICAL SUMMARY:</strong>
                                                <p>{{$response->clinical_summary}}</p>
                                                
                                            </div> 
                                            <div class="col-sm-6 form-group">
                                                <strong>MACROSCOPIC APPEARANCE:</strong>
                                               	<p>{{$response->ma_appearance}}</p>
                                            </div> 
                                            <div class="col-sm-6 form-group">
                                                <strong>MICROSCOPIC APPEARANCE:</strong>
                                                <p>{{$response->mi_appearance}}</p>

                                            </div>  
                                            <div class="col-sm-6 form-group">
                                                <strong>CONCLUSION:</strong>
                                                <p>{{$response->conclusion}}</p>

                                            </div>
                                            
                                             <div class="form-group form-row col-sm-6">
                                                    <div class="col-sm-2">
                                                        <strong>Uploaded Document: </strong>
                                                    </div>
                                                    <div class="col-sm-10">
                                                        @if($response->document != "")
                                                        <p>{{$response->document}}</p>
                                                       
                                                        @else
                                                            <p>No file available</p>
                                                        @endif
                                                        <!-- <input type="file" name="report_doc" required> -->
                                                        
                                                    </div>
                                                </div>      
                                                <div class="col-sm-6 form-group">
                                                <strong>COMMENTS:</strong>
                                                <p>{{$response->comments}}</p>

                                            </div> 
                                             <div class="form-group col-sm-6 row">
                                                <div class="col-sm-4">
                                                    <label><strong>PATHOLOGIST'S NAME:</strong></label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <p>{{$response->pathologist_name}}</p>
                                                               
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <div class="col-sm-4">
                                                    <label><strong>DATE OF REPORT:</strong></label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <p>{{$response->date_of_report}}</p>
                                                               
                                                </div>
                                            </div>
                                            
                                               <div class="col-sm-12" style="padding:30px; background: #374767; color: white; margin-top: 30px;">
                                                    <b>Important Note:</b> The original handwritten signature and date of report by Pathologist are not hosted in the Electronic Health Record System. They are available as hard copy at the laboratory or as scanned copies upon request.
                                                    
                                                </div>  
                                     </div>
              