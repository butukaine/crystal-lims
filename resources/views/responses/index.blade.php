@extends('layouts.master')
    
@section('content')

<div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">{{session('title')}}</h3>
                </div>
                <div class="content-header-right col-md-8 col-12">
                    <div class="breadcrumbs-top float-md-right">
                        <div class="breadcrumb-wrapper mr-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a>
                                </li>
                                <li class="breadcrumb-item active">Reports
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>



<div class="content-body">
      <!-- Zero configuration table -->
      <section id="configuration">

<div class="row">
    <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">List</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                         <a type="button" class="btn btn-outline-secondary" href="{{ route('download_responses')}}">
                                            <i class="ft-arrow-up-right"></i>
                                            Export
                                        </a>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <div class="table-responsive">
                                          <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                            <div class="row"><div class="col-sm-12">
                                            <table class="table table-striped table-bordered zero-configuration dataTable" id="datatable" role="grid" aria-describedby="DataTables_Table_0_info">
                                                <thead>
                                                    <tr role="row">
                                                    <th>REQUEST NO</th
                                                    ><th>BIOPSY NO</th>
                                                    <th>CLINICAL SUMMARY</th>
                                                    <th>PATHOLOGIST NAME</th>
                                                    <th >DATE OF REPORT</th>
                                                    <th></th>
                                                    <th></th>
                                                   

                                                    </tr>
                                                </thead>
                                                <tbody>
                                              @foreach($responses as $resp)
                                        <tr>
                                             <td>#{{$resp->request_id}}</td>
                                             <td>{{$resp->biopsy_no}}</td>
                                             <td>{{$resp->clinical_summary}}</td>
                                             <td>{{$resp->pathologist_name}}</td>
                                             <td>{{$resp->date_of_report}}</td>
                                             <td>
                                               @if($resp->submitted)
                                                  <span class="badge badge-light">Submitted</span>
                                                @else
                                                  <form method="POST" action="{{route('submit_response', $resp->id)}}">
                                                      {{ csrf_field()}}
                                                      <input type="hidden" name="case_id" value="{{$resp->id}}" />
                                                      <input type="submit" attr-title="{{$resp->id}}"  class="btn btn-info btn-s" data-toggle="tooltip" data-placement="left" title="Click to submit report" value="Submit">
                                                  </form>
                                                @endif
                                             </td>
                                             <td class="btn-group">
                                              <div class="btn-group">
                                                <button class="btn btn-light dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Actions
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 40px, 0px);">
                                               <a href="/reports/show/{{$resp->id}}" class="dropdown-item" title="View">View</a>
                                               <a href="/reports/edit/{{$resp->id}}" class="dropdown-item" title="View">Edit</a>
                                             <form action="{{ route('delete_response', $resp->id)}}" method="post">
                                                  @csrf
                                                  @method('DELETE')
                                                 @if(Auth::user()->role == "admin" || Auth::user()->role == "Low Level Admin")

                                                     <a class="dropdown-item" type="submit">Delete</a>
                                                     @endif
                                                </form>
                                            </div>
                                               
                                              </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                                    
                                                    
                                                </tbody>
                                               
                                            </table></div></div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </section>
              </div>
              

                    <script>
                    $('#datatable').DataTable( {
                        responsive: true
                    } );
                    </script>


                                        
@endsection


