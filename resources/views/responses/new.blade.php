@extends('layouts.master')

@section('content')

<div class="content-header row">
    <div class="content-header-left col-md-4 col-12 mb-2">
        <h3 class="content-header-title">
    {{session('title')}}</h3>
    </div>
    <div class="content-header-right col-md-8 col-12">
        <div class="breadcrumbs-top float-md-right">
            <div class="breadcrumb-wrapper mr-1">
                    <ol class="breadcrumb">
                   
                    <li class="breadcrumb-item"><a href="/dashboard">Home</a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{route('index_responses')}}">
                       Reports
                    </a>
                    </li> 
                    <li class="breadcrumb-item active">{{session('title')}}
                    </li>  
                </ol>
              
            </div>
        </div>
    </div>
</div>

<div class="content-body">
            <!-- Zero configuration table -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">
                                    Prepare Report
                                </h4>
                               <!--  <div class="heading-elements">
                                   <a href="/supervisors/new" class="btn btn-success btn-min-width box-shadow mr-1 mb-1 white"><i class="la la-plus font-medium-3"></i>New</a> 
                                </div> -->
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    @if(Session::has('message'))
                                    <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-dismissible mb-2" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        {{ Session::get('message') }}
                                    </div>
                                    @endif

                                    <div class="card-body">
                                         <form class="col-sm-12" action="{{ route('store_response')}}" method="POST" enctype="multipart/form-data" autocomplete="off">
                                            @csrf
                                            <input type="hidden" name="request_id" value="{{$request->id}}" />


                                             <h4 class="form-section text-primary">
                                            <i class="ft-user primary"></i> PATIENT DETAILS</h4>
                                            <div class="row">
                                                <div class="col-4">
                                                    <label>NAME OF PATIENT<span class="text-danger">*</span></label>

                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="text" class="form-control" required name="patient_name" value="{{$case->patient_name}}"> 
                                                        <div class="form-control-position">
                                                            <i class="ft-user"></i>
                                                        </div>
                                                         @error('patient_name')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>
                                                </div>
                                                <div class="col-4">
                                                    <label>PATIENT'S I.D NUMBER</label>
                                                     <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="text" class="form-control" name="patient_number" value="{{$case->patient_number}}">
                                                        <div class="form-control-position">
                                                            <i class="ft-user"></i>
                                                        </div>
                                                         @error('patient_number')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>
                                                </div>
                                                <div class="col-4">
                                                    <label>AGE</label>                                               
                                                     <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="text" class="form-control" name="age" value="{{$case->age}}">

                                                        <div class="form-control-position">
                                                            <i class="ft-user"></i>
                                                        </div>
                                                        @error('title')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>
                                                    
                                                </div>
                                                
                                                
                                            </div>
                                            <div class="row">
                                                <div class="col-4">
                                                    <label>GENDER</span></label>
                                                    <fieldset class="form-group">
                                                         @if($case->gender == "Male")
                                                            <label class="radio-inline"><input name="gender" value="Male" required type="radio" checked>Male</label> 
                                                            <label class="radio-inline"><input name="gender" value="Female" type="radio">Female</label>
                                                        @else
                                                            <label class="radio-inline"><input name="gender" value="Male" type="radio">Male</label> 
                                                            <label class="radio-inline"><input name="gender" value="Female" type="radio" checked>Female</label>
                                                        @endif
                                                    

                                                    </fieldset>
                                                </div>
                                                
                                                <div class="col-4">
                                                    <label>VILLAGE/ZONE</label>                                               
                                                     <fieldset class="form-group position-relative has-icon-left">
                                                         <input type="text" class="form-control" name="village" value="{{$case->village}}">

                                                        <div class="form-control-position">
                                                            <i class="ft-user"></i>
                                                        </div>
                                                        @error('title')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>
                                                    
                                                </div>
                                                <div class="col-4">
                                                    <label>PARISH</label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                         <input type="text" class="form-control" name="parish" value="{{$case->parish}}">
                                                        <div class="form-control-position">
                                                            <i class="ft-navigation"></i>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4">
                                                     <label>SUB-COUNTY</label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                       <input type="text" class="form-control" name="sub_county" value="{{$case->sub_county}}">
                                                        <div class="form-control-position">
                                                            <i class="ft-mail"></i>
                                                        </div>
                                                        @error('email')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>  
                                                </div>
                                                <div class="col-4">
                                                    <label>DISTRICT</label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="text" class="form-control" name="district" value="{{$case->district}}">
                                                        <div class="form-control-position">
                                                            <i class="ft-lock"></i>
                                                        </div>
                                                        @error('password')
                                                            <span id="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>
                                                </div>
                                                
                                            </div>
                                          


                                             <h4 class="form-section text-primary">
                                            <i class="ft-user primary"></i>PHYSICIAN DETAILS</h4>
                                            <div class="row">
                                                <div class="col-4">
                                                    <label>NAME OF PHYSICIAN</label>

                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="text" class="form-control" name="physician_name">
                                                        <div class="form-control-position">
                                                            <i class="ft-user"></i>
                                                        </div>
                                                         @error('patient_name')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>
                                                </div>
                                                <div class="col-4">
                                                    <label>CONTACT ADDRESS</label>
                                                     <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="text" class="form-control" name="physician_contact">
                                                        <div class="form-control-position">
                                                            <i class="ft-user"></i>
                                                        </div>
                                                         @error('patient_number')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>
                                                </div>
                                                <div class="col-4">
                                                    <label>PHONE CONTACT</label>                                               
                                                     <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="number" class="form-control" name="physician_mobile">  

                                                        <div class="form-control-position">
                                                            <i class="ft-user"></i>
                                                        </div>
                                                        @error('title')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>
                                                    
                                                </div>
                                                
                                                
                                            </div>
                                            <div class="row">
                                                <div class="col-4">
                                                    <label>NATURE OF SPECIMEN<span class="text-danger">*</span></label>
                                                    <fieldset class="form-group">
                                                        <select class="form-control form-control select2-selection select2-selection--single" required="" name="specimen" placeholder="Select">
                                                            <option selected value="" disabled>Select</option>
                                                            @if($case->specimen == "Histology")
                                                            <option value="Histology" selected="">Histology</option>
                                                            <option value="Cytology">Cytology</option>
                                                            @else
                                                            <option value="Histology">Histology</option>
                                                            <option value="Cytology" selected="">Cytology</option>
                                                            @endif
                                                        </select>    
                                                    

                                                    </fieldset>
                                                </div>
                                                
                                                <div class="col-4">
                                                    <label>DATE OF RECEIPT<span class="text-danger">*</span></label>                                               
                                                     <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="date" class="form-control" required name="date_of_receipt"> 
                                                        <div class="form-control-position">
                                                            <i class="ft-user"></i>
                                                        </div>
                                                        @error('title')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>
                                                    
                                                </div>
                                                <div class="col-4">
                                                    <label>HEALTH UNIT<span class="text-danger">*</span></label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                         <input type="text" class="form-control" required name="health_unit">  
                                                        <div class="form-control-position">
                                                            <i class="ft-navigation"></i>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4">
                                                     <label>BIOPSY NO.<span class="text-danger">*</span></label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                       <input type="text" class="form-control" required name="biopsy_no"> 
                                                        <div class="form-control-position">
                                                            <i class="ft-mail"></i>
                                                        </div>
                                                        @error('email')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>  
                                                </div>
                                               
                                            </div>

                                             <h4 class="form-section text-primary">
                                            <i class="ft-user primary"></i>PATHOLOGY REPORT</h4>
                                            <div class="row">
                                                <div class="col-6">
                                                     <label>CLINICAL SUMMARY<span class="text-danger">*</span></label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <textarea  class="form-control" rows="3" name="clinical_summary" required="">{{$case->clinical_summary}}</textarea>
                                                        <div class="form-control-position">
                                                            <i class="ft-mail"></i>
                                                        </div>
                                                        @error('email')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>  
                                                </div>
                                                 <div class="col-6">
                                                     <label>MACROSCOPIC APPEARANCE<span class="text-danger">*</span></label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <textarea  class="form-control" rows="3" name="macroscopic" required=""></textarea>
                                                        <div class="form-control-position">
                                                            <i class="ft-mail"></i>
                                                        </div>
                                                        @error('email')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>  
                                                </div>
                                               
                                            </div>
                                            <div class="row">
                                                <div class="col-6">
                                                     <label>MICROSCOPIC APPEARANCE<span class="text-danger">*</span></label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <textarea  class="form-control" rows="3" name="microscopic" required=""></textarea>
                                                        <div class="form-control-position">
                                                            <i class="ft-mail"></i>
                                                        </div>
                                                        @error('email')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>  
                                                </div>
                                                 <div class="col-6">
                                                     <label>CONCLUSION<span class="text-danger">*</span></label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <textarea  class="form-control" rows="3" name="conclusion" required></textarea>
                                                        <div class="form-control-position">
                                                            <i class="ft-mail"></i>
                                                        </div>
                                                        @error('email')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>  
                                                </div>
                                               
                                            </div>

                                            <div class="row">
                                                <div class="col-6">
                                                     <label>UPLOAD DOCUMENT</label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                      <input type="file" name="report_doc">

                                                       
                                                        @error('email')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>  
                                                </div>
                                                 <div class="col-6">
                                                     <label>COMMENTS</label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                         <textarea  class="form-control" rows="3" name="comments"></textarea>
                                                        <div class="form-control-position">
                                                            <i class="ft-mail"></i>
                                                        </div>
                                                        @error('email')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>  
                                                </div>
                                               
                                            </div>
                                             <div class="row">
                                                <div class="col-6">
                                                     <label>PATHOLOGIST'S NAME<span class="text-danger">*</span></label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="text" class="form-control" name="pathologist_name" >
                                                        <div class="form-control-position">
                                                            <i class="ft-mail"></i>
                                                        </div>
                                                        @error('email')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>  
                                                </div>
                                                 <div class="col-6">
                                                     <label>DATE OF REPORT<span class="text-danger">*</span></label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                       <input type="date" class="form-control" required name="date_of_report">   
                                                        <div class="form-control-position">
                                                            <i class="ft-mail"></i>
                                                        </div>
                                                        @error('email')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>  
                                                </div>
                                               
                                            </div>
                                             <div class="form-actions">
                                                <a href="{{route('index_responses')}}" class="btn btn-danger mr-1 white">
                                                    <i class="ft-x"></i> Cancel
                                                </a>
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check"></i> Save
                                                </button>
                                            </div>

                                        </form>
                                    </div>
                                    
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->

            </div>

@endsection