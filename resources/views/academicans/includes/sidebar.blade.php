<aside class="main-sidebar">
  <!-- sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="image pull-left">
        <img src="{{ asset('assets/dist/img/avatar5.png') }}" class="img-circle" alt="User Image">
      </div>
      <div class="info">
        <h4>Welcome</h4>
        <p>{{ Sentinel::getUser()->last_name }} {{ Sentinel::getUser()->first_name }}</p>
      </div>
    </div>

    <!-- sidebar menu -->
    <ul class="sidebar-menu">
      <li class="@if(Request::is('academican/dashboard')) active  @endif">
        <a href="{{ route('academicans.dashboard') }}"><i class="fa fa-hospital-o"></i><span>Dashboard</span>
        </a>
      </li>
      <li class="@if(Request::is('admin/profile')) active  @endif">
        <a href="{{-- {{ route('academican_profile') }} --}}">
         <i class="fa fa-gear"></i><span> My Profile</span>
       </a>
     </li>
   </ul>
