<head>
  <style type="text/css">
    table, th, td {
    border: 0.5px solid green;
      border-collapse: collapse;
}
th, td {
  padding: 15px;
}
table {
  border-spacing: 5px;
}
  </style>
</head>
  <body>
    <div>
      <img class="text-center" alt="Logo" src="{{public_path('/logo-red.png')}}"
                     width=230" height="100"> 
          <h1>List of Requests</h1>
          <p>Download Date: {{$date}}</p>
          <hr>
    </div>
  <div class="table-responsive" style="padding-top: 15px;">
                                <table id="datatable" class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline">  
                                    <thead>
                                        <tr>
                                            <th>CASE CODE</th
                                                    ><th>SUBMITTED BY</th>
                                                    <th>PATIENT NANE</th>
                                                    <th>DATE SUBMITTED</th>
                                                    <th>PREPARE REPORT</th>
                                                  
                                        </tr>
                                    </thead>
                                    <tbody>
                                       @foreach($requests as $req)
                                        <tr>
                                            <td>{{$req->case->code}}</td>
                                             <td>{{$req->clinician->name}}</td>
                                             <td>{{$req->case->patient_name}}</td>
                                             <td>{{$req->date_submitted}}</td>
                                             <td class="">
                                                @if($req->responded > 0)
 <p class="label-default label label-secondary" style="color: green"> Report Available</p>

                                                @else
                                                      <p class="label-default label label-secondary" style="color: red"> Start to Prepare</p>

                                                @endif
                                             </td>
                                      
                                        </tr>
                                        @endforeach
                                   
                </tbody>
            </table>
        </div>
  </body>
