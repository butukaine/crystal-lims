@extends('layouts.master')
    
@section('content')
<div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">{{session('title')}}</h3>
                </div>
                <div class="content-header-right col-md-8 col-12">
                    <div class="breadcrumbs-top float-md-right">
                        <div class="breadcrumb-wrapper mr-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a>
                                </li>
                                <li class="breadcrumb-item active">Requests
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>



<div class="content-body">
                <!-- Zero configuration table -->
                <section id="configuration">
<div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">List of Requests</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <a type="button" class="btn btn-outline-secondary" href="{{ route('download_requests')}}">
                                <i class="ft-arrow-up-right"></i>
                                Export
                            </a>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <div class="table-responsive">
                                            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                            
                                            
                                            <div class="row"><div class="col-sm-12">
                                            <table class="table table-striped table-bordered zero-configuration dataTable" id="datatable" role="grid" aria-describedby="DataTables_Table_0_info">
                                                <thead>
                                                    <tr role="row">
                                                    <th>CASE CODE</th
                                                    ><th>SUBMITTED BY</th>
                                                    <th>PATIENT NANE</th>
                                                    <th>DATE SUBMITTED</th>
                                                    <th>CASE DOCUMENT</th>
                                                    <th>PREPARE REPORT</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                               @foreach($requests as $req)
                                        <tr>
                                             
                                             <td>{{$req->case->code}}</td>
                                             <td>{{$req->clinician->name}}</td>
                                             <td>{{$req->case->patient_name}}</td>
                                             <td>{{$req->date_submitted}}</td>
                                             <td> @if($req->case->document != "")
                                                        <form action="{{route('download')}}" method="GET">
                                                            @csrf
                                                            <input type="hidden" name="path" value='{{$req->case->document}}'>
                                                            <input type="submit" class="btn btn-warning btn-s" value="Download">
                                                        </form>
                                                        @else
                                                            <p class="text-secondary">Not Available</p>
                                                        @endif  
                                              </td>

                                             <td class="">
                                                @if($req->responded > 0)
                                                  <!-- <span class="label-default label label-warning">Responded</span> -->
                                                  <p><a href="/reports/show/{{$req->response_id}}" target="_blank" class="btn btn-default btn-s">View Report</a></p>

                                                @else
                                                     @if(Auth::user()->role == "Health Facility User" || Auth::user()->role == "Clinician")
                                                      @else
                                                      <a href="/reports/new/request_id={{$req->id}}"  class="btn btn-info btn-s" data-toggle="tooltip" data-placement="left" title="Click to prepare report">  START</a>
                                                      @endif 
                                                @endif
                                             </td>
                                             
                                        </tr>
                                        @endforeach
                                                    
                                                </tbody>    
                                               
                                            </table></div></div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </section>
              </div>
              

                    <script>
                    $('#datatable').DataTable( {
                        responsive: true
                    } );
                    </script>


                                        
@endsection


