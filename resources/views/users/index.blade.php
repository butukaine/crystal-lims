@extends('layouts.master')
    
@section('content')
<div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">{{session('title')}}</h3>
                </div>
                <div class="content-header-right col-md-8 col-12">
                    <div class="breadcrumbs-top float-md-right">
                        <div class="breadcrumb-wrapper mr-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a>
                                </li>
                                <li class="breadcrumb-item active">{{session('title')}}
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>



<div class="content-body">
                <!-- Zero configuration table -->
                <section id="configuration">
<div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Registered {{session('title')}}</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                 
                                       
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <div class="table-responsive">
                                            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                            
                                            
                                            <div class="row"><div class="col-sm-12">
                                            <table class="table table-striped table-bordered zero-configuration dataTable" id="datatable" role="grid" aria-describedby="DataTables_Table_0_info">
                                                <thead>
                                                    <tr role="row">
                                            <th>NAME</th>
                                            <th>EMAIL</th>
                                            <th>PHONE</th>
                                            <th>STATUS</th>
                                            <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                               @foreach($users as $c)
                                        <tr>
                                             
                                             <td>{{$c->name}}</td>
                                             <td>{{$c->email}}</td>
                                             <td>{{$c->phone}}</td>
                                             
                                                @if($c->account_state == 0)
                                                <td>
                                                <span class="badge badge-pill badge-danger">Disabled</span>
                                                </td>
                                                <td>
                                                      <form method="POST" action="{{route('activate')}}">
                                                              {{ csrf_field() }}
                                                              <input type="hidden" name="user_id" value="{{$c->id}}" />
                                                              <input type="submit" attr-title="{{$c->id}}"  class="btn btn-success btn-rounded btn-xs" data-toggle="tooltip" data-placement="left" value=" Activate">
                                                            </form>  
                                                </td>
                                                
                                                 
                                                @else
                                                <td>
                                                <span class="badge badge-pill badge-success">Active</span>
                                                </td>
                                                <td>
                                                     <form method="POST" action="{{route('deactivate')}}">
                                                          {{ csrf_field() }}
                                                          <input type="hidden" name="user_id" value="{{$c->id}}" />
                                                          <input type="submit" attr-title="{{$c->id}}"  class="btn btn-light text-white" data-toggle="tooltip" data-placement="left" value=" Disable">
                                                        </form>
                                                </td>
                                               
                                                @endif
                                        
                                            
                                             
                                        </tr>
                                        @endforeach
                                                    
                                                </tbody>    
                                               
                                            </table></div></div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </section>
              </div>
              

                    <script>
                    $('#datatable').DataTable( {
                        responsive: true
                    } );
                    </script>


                                        
@endsection


