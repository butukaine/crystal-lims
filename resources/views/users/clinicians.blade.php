@extends('layouts.master')
    
@section('content')
<div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">{{session('title')}}</h3>
                </div>
                <div class="content-header-right col-md-8 col-12">
                    <div class="breadcrumbs-top float-md-right">
                        <div class="breadcrumb-wrapper mr-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a>
                                </li>
                                <li class="breadcrumb-item active">Clinicians
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>



<div class="content-body">
                <!-- Zero configuration table -->
                <section id="configuration">
<div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Registered Clinicians</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <a class="btn btn-success" href=""> <i class="fa fa-plus"></i>
                                      Export </a>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <div class="table-responsive">
                                            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                            
                                            
                                            <div class="row"><div class="col-sm-12">
                                            <table class="table table-striped table-bordered zero-configuration dataTable" id="datatable" role="grid" aria-describedby="DataTables_Table_0_info">
                                                <thead>
                                                    <tr role="row">
                                                     <th>PHOTO</th>
                                            <th>NAME</th>
                                            <th>EMAIL</th>
                                            <th>PHONE</th>
                                            <th>DATE OF BIRTH</th>
                                            <th>ADDRESS</th>
                                            <th>ACCOUNT STATUS</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                               @foreach($clinicians as $c)
                                        <tr>
                                             
                                             <td></td>
                                             <td>{{$c->name}}</td>
                                             <td>{{$c->email}}</td>
                                             <td>{{$c->phone}}</td>
                                             <td>{{$c->dob}}</td>
                                             <td>{{$c->address}}</td>
                                             <td>
                                                @if($c->account_state == 0)
                                                    <div>
                                                            <p><em>User is disabled</em></p>
                                                        
                                                            <form method="POST" action="{{route('activate')}}">
                                                              {{ csrf_field() }}
                                                              <input type="hidden" name="user_id" value="{{$c->id}}" />
                                                              <input type="submit" attr-title="{{$c->id}}"  class="btn btn-primary btn-rounded btn-xs" data-toggle="tooltip" data-placement="left" title="Click to activate account" value=" ACTIVATE">
                                                            </form>                                                        
                                                    </div>
                                                 
                                                @else
                                                <div>
                                                        <p><em>User is online</em></p>
                                                    
                                                        <form method="POST" action="{{route('deactivate')}}">
                                                          {{ csrf_field() }}
                                                          <input type="hidden" name="user_id" value="{{$c->id}}" />
                                                          <input type="submit" attr-title="{{$c->id}}"  class="btn btn-danger btn-rounded btn-xs" data-toggle="tooltip" data-placement="left" title="Click to activate account" value=" DE-ACTIVATE">
                                                        </form>                                                        
                                                </div>
                                                @endif
                                            </td>
                                             
                                        </tr>
                                        @endforeach
                                                    
                                                </tbody>    
                                               
                                            </table></div></div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </section>
              </div>
              

                    <script>
                    $('#datatable').DataTable( {
                        responsive: true
                    } );
                    </script>


                                        
@endsection


