@extends('layouts.master')


@section('content')
  <!-- Content Header (Page header) -->
                <section class="content-header">
                     <form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
                            <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </form>   
                    <div class="header-icon">
                        <i class="fa fa-users"></i>
                    </div>
                    <div class="header-title">
                        <h1> Academicians</h1>
                        <small></small>
                        <ol class="breadcrumb hidden-xs">
                            <li><a href="/home"><i class="pe-7s-home"></i> Home</a></li>
                            <li class="active">Academicians</li>
                        </ol>
                    </div>
                </section>
            <!-- Main content -->
                <section class="content">
                  <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-bd" data-inner-id="a7HgTIWv4t" data-index="0">
                                    <div class="panel-heading">
                                        <div class="btn-group"> 
                                            <span class="btn btn-success"> <i class="fa fa-list"></i>  Registered Academicians</span>  
                                        </div>
                                    </div>
                                    <div class="panel-body">
                              <div class="table-responsive">
                              <table id="datatable" class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline">  
                                    <thead>
                                        <tr>
                                            <th>PHOTO</th>
                                            <th>NAME</th>
                                            <th>EMAIL</th>
                                            <th>PHONE</th>
                                            <th>DATE OF BIRTH</th>
                                            <th>ADDRESS</th>
                                            <th>ACCOUNT STATUS</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($academicians as $c)
                                        <tr>
                                             <td></td>
                                             <td>{{$c->name}}</td>
                                             <td>{{$c->email}}</td>
                                             <td>{{$c->phone}}</td>
                                             <td>{{$c->dob}}</td>
                                             <td>{{$c->address}}</td>
                                             <td>
                                                @if($c->account_state == 0)
                                                    <div>
                                                            <p><em>User is disabled</em></p>
                                                        
                                                            <form method="POST" action="{{route('activate')}}">
                                                              {{ csrf_field() }}
                                                              <input type="hidden" name="user_id" value="{{$c->id}}" />
                                                              <input type="submit" attr-title="{{$c->id}}"  class="btn btn-primary btn-rounded btn-xs" data-toggle="tooltip" data-placement="left" title="Click to activate account" value=" ACTIVATE">
                                                            </form>
                                                        
                                                    </div>
                                                 
                                                @else
                                                    <div>
                                                            <p><em>User is online</em></p>
                                                            <form method="POST" action="{{route('deactivate')}}">
                                                              {{ csrf_field() }}
                                                              <input type="hidden" name="user_id" value="{{$c->id}}" />
                                                              <input type="submit" attr-title="{{$c->id}}"  class="btn btn-danger btn-rounded btn-xs" data-toggle="tooltip" data-placement="left" title="Click to activate account" value=" DE-ACTIVATE">
                                                            </form>
                                         
                                                    </div>
                                                @endif
                                            </td>
                                             
                                        </tr>
                                        @endforeach
                                   
					                </tbody>
					            </table>
        </div>
    </div>
</div>
</div>
</div> 
</section> 
<!-- /.content -->

<script text="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable({
            columnDefs: [{ orderable: false, targets: [6] }],

        });
    });
</script>
@endsection