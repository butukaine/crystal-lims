<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'WebsiteController@index')->name('landing_page');


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/logout', 'HomeController@logout')->name('logout');
Route::get('/download', 'HomeController@download')->name('download');
Route::get('/termsandconditions', 'OtherPagesController@terms')->name('terms');
Route::get('/display/{path}', 'HomeController@displayFile')->name('display_file');
Route::get('/profile', 'UserController@profile')->name('profile');
Route::get('/get_bar_graph_stats', 'HomeController@getStats');


Route::group(['prefix' => 'cases', 'middleware' => ['auth', 'case']], function(){
  Route::get('/', 'CaseController@index')->name('index_cases');
  Route::get('/new', 'CaseController@create')->name('create_case');
  Route::post('/new', 'CaseController@store')->name('store_case');
  Route::post('/upload', 'CaseController@storeDoc')->name('store_doc');
  Route::get('/pdf', 'CaseController@pdf')->name('get_pdf');
  Route::get('/show/{id}', 'CaseController@show')->name('show_case');
  Route::get('/edit/{id}', 'CaseController@edit')->name('edit_case');
  Route::patch('/update/{id}', 'CaseController@update')->name('update_case');
  Route::delete('/delete/{id}', 'CaseController@destroy')->name('delete_case');
  Route::get('/downloadList', 'CaseController@downloadList')->name('download_list');
  Route::get('/downloadCase/{id}', 'CaseController@downloadCase')->name('download_case');

});


Route::group(['prefix' => 'requests','middleware' => ['auth', 'case']], function(){
  Route::get('/', 'RequestController@index')->name('index_requests');
  Route::get('/new', 'RequestController@create')->name('create_request');
  Route::post('/new', 'RequestController@store')->name('store_request');
  Route::delete('/delete/{id}', 'RequestController@destroy')->name('delete_request');
  Route::get('/downloadList', 'RequestController@downloadList')->name('download_requests');

});

Route::group(['prefix' => 'reports','middleware' => ['auth', 'case']], function(){
  Route::get('/', 'ResponseController@index')->name('index_responses');
  Route::get('/new/request_id={request}', 'ResponseController@create')->name('create_response');
  Route::post('/new', 'ResponseController@store')->name('store_response');
  Route::post('/submit/{id}', 'ResponseController@submit')->name('submit_response');
  Route::get('/show/{id}', 'ResponseController@show')->name('show_response');
  Route::get('/edit/{id}', 'ResponseController@edit')->name('edit_response');
  Route::patch('/update/{id}', 'ResponseController@update')->name('update_response');
  Route::delete('/delete/{id}', 'ResponseController@destroy')->name('delete_response');
  Route::get('/downloadList', 'ResponseController@downloadList')->name('download_responses');
  Route::get('/downloadResponse/{id}', 'ResponseController@downloadResponse')->name('download_response');

});

Route::group(['prefix' => 'health_facilities','middleware' => ['auth', 'admin']], function(){
  Route::get('/', 'HealthFacilityController@index')->name('index_health_facility');
  Route::get('/new/', 'HealthFacilityController@create')->name('create_health_facility');
  Route::post('/store', 'HealthFacilityController@store')->name('store_health_facility');

});

Route::group(['middleware' => ['auth','admin']], function(){
  Route::get('/clinicians', 'UserController@index_clinicians')->name('index_clinicians');
  Route::get('/hfusers', 'UserController@index_hfusers')->name('index_hfusers');
  Route::get('/academicians', 'UserController@index_academicians')->name('index_academicians');
  Route::get('/admin', 'UserController@index_admin')->name('index_admin');
  Route::post('/activate', 'UserController@activate')->name('activate');
  Route::post('/deactivate', 'UserController@deactivate')->name('deactivate');
});
//Route::group(['middleware' => ['auth']], function(){
  Route::get('/search', 'SearchController@start')->name('start_search');
  Route::get('/mysearches', 'SearchController@mysearches')->name('mysearches');
  Route::get('/see_results/{id}', 'SearchController@seeResults')->name('see_results');
  Route::post('/search', 'SearchController@search')->name('search');
  Route::post('/store_academican', 'SearchController@storeAcademician')->name('store_academician_search');
//});

Route::post('/approve_search', 'SearchResultsController@approveSearch');
Route::get('/add_results/{search_id}', 'SearchResultsController@addResults');
Route::post('/store_result', 'SearchResultsController@storeResult')->name('store_search_result');
Route::get('/get_results/{search_id}', 'SearchResultsController@getResults');
Route::get('/myresults/{search_id}', 'SearchResultsController@getAcademicianResults');

  Route::get('/search_results', 'SearchResultsController@viewResultsTable');
  Route::post('/enable_viewing/{id}', 'SearchResultsController@enableViewing')->name('enable_viewing');
  Route::post('/upload_report/{id}', 'SearchResultsController@uploadReport')->name('upload_report');

Route::group(['prefix' => 'searches','middleware' => ['auth']], function(){
  Route::get('/', 'SearchController@index')->name('index_search');
  Route::get('/new/', 'SearchController@create')->name('create_search');
  Route::post('/store', 'SearchController@store')->name('store_search');
  Route::post('/approve', 'SearchController@approve')->name('approve');
  Route::get('/upload_result/{id}', 'SearchController@uploadResult')->name('upload_search_result');
});

//Email
Route::get('mail', function () {
    $user = App\User::find(1);

    return (new App\Notifications\UserActivation($user))
                ->toMail($user);
});

Route::get('/sendemail', 'NotifyController@index');
Route::get('/new_user', 'NotifyController@new_user');
Route::get('/report_available', 'NotifyController@report_available');
